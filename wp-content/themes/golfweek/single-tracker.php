<?php
	$queried_object = get_queried_object();
	// $taxonomy = $queried_object->taxonomy;
	// $term_id = $queried_object->term_id;  
get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css">

<main class="single-tracker">
	<article>
		<div class="content">
			<header>
				<h3>
					<span class="story">STORY</span><br>
					<span class="tracker">TRACKER</span>
				</h3>
				<h1>
					<?php the_title(); ?>
				</h1>
				<ul class="share-article-top">
					<li class="facebook-share" data-url="<?php echo get_permalink(); ?>"><a class="link-facebook"><i class="fa fa-facebook-square"></i><span>Share</span></a></li>
					<li id="twitter-wjs"><a href="https://twitter.com/intent/tweet?text=<?php echo the_title() . '&url=' . get_permalink(); ?>" target="_blank" class="link-twitter"><i class="fa fa-twitter"></i><span>Tweet</span></a></li>
					<li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_title() . ' ' . get_permalink(); ?>" class="link-email"><i class="fa fa-envelope"></i><span>Email</span></a></li>
				</ul>
			</header>
			<div class="article-body">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>

			</div>
			<section class="right-modules">
				<?php include(locate_template('partials/module-builder.php')); ?>
			</section>
		</div>

	</article>
</main>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1646295555626359";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/swiper.jquery.min.js"></script>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<?php get_footer(); ?>