<div class="modal-inner">
	<div class="modal-content as-ad">
		<div class="modal-content-inner">
			<div class="modal-close">Close <span class="icon-golfweek-icons_close"></span></div>
			<?php echo gw_ads_get_dfp_html('modal', GW_ADS_SLOT_MODAL, '650x500'); ?>
			<div class="gw-btn-white modal-continue">Continue to Golfweek</div>
		</div>
	</div>
</div>
<div class="modal-curtain"></div>