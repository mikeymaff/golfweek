<?php
	$post = $promo_box;
	setup_postdata($post);

	// MAIN LINK
	if(get_field('main_link_internal_or_external') == 'internal') {
		$main_link_and_target = 'href="' . get_field('internal_main_link') . '"';
	} else {
		$main_link_and_target = 'href="' . get_field('external_main_link') . '" target="_blank"';
	}

	// CTA LINK
	if(get_field('include_extra_cta')) {
		if(get_field('extra_cta_internal_or_external') == 'internal') {
			$cta_link_and_target = 'href="' . get_field('internal_extra_cta_link') . '"';
		} else {
			$cta_link_and_target = 'href="' . get_field('external_extra_cta_link') . '" target="_blank"';
		}
	}
?>

<div class="promo-container">
	<?php if(has_post_thumbnail()) { ?>
		<figure>
			<a <?php echo $main_link_and_target; ?>>
				<?php the_post_thumbnail('medium'); ?>
			</a>
		</figure>
	<?php } ?>
	<h3 class="title-container">
		<a <?php echo $main_link_and_target; ?>>
			<span class="title-one"><?php the_field('title_line_one'); ?></span>
			<span class="title-two"><?php the_field('title_line_two'); ?></span>
		</a>
	</h3>
	<div class="promo-body">
		<a <?php echo $main_link_and_target; ?>>
			<?php the_content(); ?>
		</a>
	</div>
	<?php if(get_field('include_extra_cta')) { ?>
		<div class="promo-cta">
			<a <?php echo $cta_link_and_target; ?>>
				<?php the_field('extra_cta_copy'); ?> <i class="fa fa-angle-double-right"></i>
			</a>
		</div>
	<?php } ?>
</div>

<?php wp_reset_postdata(); ?>