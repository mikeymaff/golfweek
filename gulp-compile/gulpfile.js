// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();

// Gulp Config Options & Filepaths
var paths = require('./config').paths;
var options = require('./config').options;

// Compile Our Less
gulp.task('less', function() {

return gulp.src(paths.less.src)
.pipe($.plumber(options.plumber))
.pipe(less())
.pipe(concat('style.css'))
.pipe(gulp.dest('../wp-content/themes/golfweek'))
.on('error', options.plumber.errorHandler);
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
return gulp.src(paths.js.src)
.pipe(concat('all.js'))
.pipe(gulp.dest('../wp-content/themes/golfweek/dist'))
.pipe(rename('all.min.js'))
.pipe(uglify())
.pipe(gulp.dest('../wp-content/themes/golfweek/dist'));
});

// Auto Refresh Browser
browserSync.init(options.browserSync);

gulp.task('reload', function() {
  browserSync.reload();
});

// Watch Files For Changes
gulp.task('watch', function() {
gulp.watch(paths.js.watch, ['scripts', 'reload']);
gulp.watch(paths.less.watch, ['less', 'reload']);
});

// Default Task
gulp.task('default', ['less', 'scripts', 'watch']);