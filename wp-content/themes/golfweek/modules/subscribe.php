<?php

?>

<h3 class="subscribe-title"><?php the_field('subscribe_title', 'options'); ?></h3>
<p class="subscribe-body">
	<?php the_field('subscribe_body_copy', 'options'); ?>
</p>
<a class="gw-btn-white" href="<?php the_field('subscribe_url', 'options'); ?>">
	Subscribe
</a>