<?php 
	$post_date_as_class = str_replace(' ', '', $entry_date);
?>

<style>

	#liveblog-entries > .<?php echo $post_date_as_class; ?> ~ .<?php echo $post_date_as_class; ?> header.liveblog-meta .liveblog-meta-time a {
	    display:none;
	}
</style>

<div id="liveblog-entry-<?php echo esc_attr( $entry_id ); ?>" class="<?php echo esc_attr( $css_classes ) . ' ' . $post_date_as_class; ?>" data-timestamp="<?php echo esc_attr( $timestamp ); ?>">
	<header class="liveblog-meta">
		<?php /* 
		<span class="liveblog-author-avatar"><?php echo wp_kses_post( $avatar_img ); ?></span>
		<span class="liveblog-author-name"><?php echo wp_kses_post( $author_link ); ?></span>
		*/ ?>
		<?php 
			$today_date = date("M j");
			$today_year = date("Y");
		?>
		<span class="liveblog-meta-time">
			<a href="#liveblog-entry-<?php echo absint( $entry_id ); ?>">
				<?php if($today_date == $entry_date && $today_year == $entry_date_year) { ?>
					<span class="date">TODAY</span>
				<?php } else { ?>
					<span class="date"><?php echo esc_html( $entry_date ); ?></span><br>
					<span class="date-year"><?php echo esc_html( $entry_date_year ); ?></span>
				<?php } ?>
			</a>
		</span>
	</header>
	<?php $starts_with_link_class = '';
	if(0 === strpos($original_content, '<a')) {
		$starts_with_link_class = 'starts-with-link';
	} ?>
	<div class="liveblog-entry-text <?php echo $starts_with_link_class; ?>" data-original-content="<?php echo esc_attr( $original_content ); ?>">
		<?php echo $content; ?>
		<time><?php echo esc_html( $entry_time ); ?></time>
	</div>
<?php if ( $is_liveblog_editable ): ?>
	<ul class="liveblog-entry-actions">
		<li><button class="liveblog-entry-edit button-secondary"><?php esc_html_e( 'Edit', 'liveblog' ); ?></button><button class="liveblog-entry-delete button-secondary"><?php esc_html_e( 'Delete', 'liveblog' ); ?></button></li>
	</ul>
<?php endif; ?>
</div>
