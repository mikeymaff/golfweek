<?php

function isLocal () {
    return ( $_SERVER["SERVER_ADDR"] == '127.0.0.1' );
}

//LOAD STYLESHEETS AND SCRIPTS
add_action('wp_enqueue_scripts', 'golfweek_enqueue_scripts');
function golfweek_enqueue_scripts () {
    wp_enqueue_style('golfweek-style', get_stylesheet_uri());

    if(isLocal()) {
        wp_enqueue_script('js.cookie', get_template_directory_uri() . '/js/js.cookie.js', array(), false, true );
        wp_enqueue_script('app', get_template_directory_uri() . '/js/z_app.js', array(), false, true );
        wp_enqueue_script( 'ajax-gallery',  get_stylesheet_directory_uri() . '/js/ajax-gallery.js', array( 'jquery' ), '1.0', true );
    } else {
        wp_enqueue_script('myjquery', get_template_directory_uri() . '/dist/all.min.js', array(), false, true );
        wp_enqueue_script( 'ajax-gallery',  get_stylesheet_directory_uri() . '/js/ajax-gallery.js', array( 'jquery' ), '1.0', true );
    }
    wp_enqueue_style('jquery-ui-datepicker');
    wp_enqueue_script('jquery-ui-datepicker');

    global $wp_query;
    wp_localize_script( 'ajax-gallery', 'ajaxgallery', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' )
    ));
}

add_action( 'wp_ajax_nopriv_ajax_gallery', 'my_ajax_gallery' );
add_action( 'wp_ajax_ajax_gallery', 'my_ajax_gallery' );

function my_ajax_gallery() {
    $gallery_id = $_POST['gallery_id'];

    $args = array(
        'post_status'    => 'publish',
        'post_type'      => array('gallery'),
        'p'              => $gallery_id
    );
    $posts = new WP_Query($args);
    $GLOBALS['wp_query'] = $posts;

    while ( $posts->have_posts() ) { 
        $posts->the_post();
        
        include(locate_template('partials/gallery.php'));
    }

    die();
}

//CREATE STAFF TAXONOMY
function staff_init() {
    register_taxonomy(
        'staff',
        array('post', 'quick_shot', 'gallery', 'video_post'),
        array(
            'label' => __( 'Staff' ),
            'rewrite' => array( 'slug' => 'staff' )
        )
    );
}
add_action( 'init', 'staff_init' );

//CREATE PLAYER TAXONOMY
function player_init() {
    register_taxonomy(
        'player',
        array('post', 'quick_shot', 'gallery', 'video_post'),
        array(
            'label' => __( 'Player' ),
            'rewrite' => array( 'slug' => 'player' )
        )
    );
}
add_action( 'init', 'player_init' );

//CREATE NEWSLETTER TAXONOMY
function newsletter_init() {
    register_taxonomy(
        'newsletter',
        array('post', 'quick_shot', 'gallery', 'video_post'),
        array(
            'label' => __( 'Newsletter' ),
            'rewrite' => array( 'slug' => 'newsletter' )
        )
    );
}
add_action( 'init', 'newsletter_init' );

//CREATE QUICKSHOTS POST TYPE
add_action( 'init', 'create_quick_shot' );
function create_quick_shot() {
    register_post_type( 'quick_shot',
        array(
            'menu_icon' => 'dashicons-megaphone',
            'labels' => array(
                'name' => __( 'Quick Shots' ),
                'singular_name' => __( 'Quick Shot' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'editor', 'title', 'revisions'),
        )
    );
}

function get_quickshot_url($post_object) {
    return '/quick-shots#' . ($post_object -> post_name);
}

//CREATE PROMO BOX POST TYPE
add_action( 'init', 'create_promo_box' );
function create_promo_box() {
    register_post_type( 'promo_box',
        array(
            'menu_icon' => 'dashicons-media-interactive',
            'labels' => array(
                'name' => __( 'Promo Boxes' ),
                'singular_name' => __( 'Promo Box' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'editor', 'title', 'revisions'),
        )
    );
}

//CREATE GALLERY POST TYPE
add_action( 'init', 'create_gallery' );
function create_gallery() {
    register_post_type( 'gallery',
        array(
            'menu_icon' => 'dashicons-images-alt2',
            'taxonomies' => array('category', 'post_tag'),
            'labels' => array(
                'name' => __( 'Galleries' ),
                'singular_name' => __( 'Gallery' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'title', 'revisions'),
        )
    );
}

//CREATE VIDEO POST TYPE
add_action( 'init', 'create_video_post' );
function create_video_post() {
    register_post_type( 'video_post',
        array(
            'menu_icon' => 'dashicons-format-video',
            'taxonomies' => array('category', 'post_tag'),
            'labels' => array(
                'name' => __( 'Videos' ),
                'singular_name' => __( 'Video' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'title', 'revisions'),
        )
    );
}

//CREATE TRACKER POST TYPE
add_action( 'init', 'create_tracker' );
function create_tracker() {
    register_post_type( 'tracker',
        array(
            'menu_icon' => 'dashicons-clock',
            'taxonomies' => array('category', 'post_tag', 'player', 'newsletter'),
            'labels' => array(
                'name' => __( 'Trackers' ),
                'singular_name' => __( 'Tracker' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'title'),
        )
    );
}

//MODULE CONFIGURATION

if(function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title'    => 'Module Config',
        'menu_title'    => 'Module Config',
        'menu_slug'     => 'module-config',
        'capability'    => 'edit_posts',
        'redirect'      => true,
        'position'      => 40
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Newsletter Sign Up',
        'menu_title'    => 'Newsletter Sign Up',
        'parent_slug'   => 'module-config',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Magazine Subscribe',
        'menu_title'    => 'Magazine Subscribe',
        'parent_slug'   => 'module-config',
    ));

}

//GLOBAL OPTIONS

if(function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title'    => 'Global Options',
        'menu_title'    => 'Global Options',
        'menu_slug'     => 'global-options',
        'icon_url'      => 'dashicons-admin-site',
        'capability'    => 'edit_posts',
        'redirect'      => true,
        'position'      => 4

    ));

    // add sub page
    acf_add_options_sub_page(array(
        'page_title'    => 'Header',
        'menu_title'    => 'Header',
        'parent_slug'   => 'global-options',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => '404',
        'menu_title'    => '404',
        'parent_slug'   => 'global-options',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Default Modules',
        'menu_title'    => 'Default Modules',
        'parent_slug'   => 'global-options',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Footer',
        'menu_title'    => 'Footer',
        'parent_slug'   => 'global-options',
    ));
}


//REMOVE COMMENTS PAGE FROM ADMIN PANEL
function custom_menu_page_removing() {
    remove_menu_page('edit-comments.php');
}
add_action( 'admin_menu', 'custom_menu_page_removing' );


//ADD FEATURED IMAGE SUPPORT
add_theme_support('post-thumbnails');

//ADD RULE TO SECTION
add_filter('acf/location/rule_types', 'acf_location_rules_types');
function acf_location_rules_types( $choices )
{
    $choices['Other']['taxonomy_depth'] = 'Taxonomy Depth';
    $choices['Terms'] = array();
    $choices['Terms']['category_id'] = 'Category Name';
    return $choices;
}

//MATCHING OPERATORS
add_filter('acf/location/rule_operators', 'acf_location_rules_operators');
function acf_location_rules_operators( $choices )
{
    //BY DEFAULT WE HAVE == AND !=
    $choices['<'] = 'is less than';
    $choices['>'] = 'is greater than';

    return $choices;
}

//POPULATE LIST WITH OPTIONS
add_filter('acf/location/rule_values/taxonomy_depth', 'acf_location_rules_values_taxonomy_depth');
function acf_location_rules_values_taxonomy_depth( $choices )
{

    for ($i=0; $i < 4; $i++)
    {
        $choices[$i] = $i;
    }

    return $choices;
}

//MATCH THE RULE
add_filter('acf/location/rule_match/taxonomy_depth', 'acf_location_rules_match_taxonomy_depth', 10, 3);
function acf_location_rules_match_taxonomy_depth( $match, $rule, $options )
{
    $depth = (int) $rule['value'];

    if(isset($_GET['taxonomy']) && isset($_GET['tag_ID']))
    {
        $term_depth = (int) count(get_ancestors($_GET['tag_ID'], $_GET['taxonomy']));
    } else {
        $term_depth = 100;
    }

    if($rule['operator'] == "==")
    {
        $match = ($term_depth == $depth);
    }
    elseif($rule['operator'] == "!=")
    {
        $match = ($term_depth != $depth);
    }
    elseif($rule['operator'] == "<")
    {
        $match = ($term_depth < $depth);
    }
    elseif($rule['operator'] == ">")
    {
        $match = ($term_depth > $depth);
    }

    return $match;
}

// ENABLE CATEGORY SELECT FOR ACF
add_filter('acf/location/rule_values/category_id', 'acf_location_rules_values_category');
function acf_location_rules_values_category($choices) {
    // get terms and build choices
    $taxonomy = 'category';
    $args = array('hide_empty' => false);
    $terms = get_terms($taxonomy, $args);
    if (count($terms)) {
        foreach ($terms as $term) {
            $choices[$term->term_id] = $term->name;
        }
    }
    return $choices;
}

add_filter('acf/location/rule_match/category_id', 'acf_location_rules_match_category', 10, 3);
function acf_location_rules_match_category($match, $rule, $options) {
    if (!isset($_GET['tag_ID']) || 
            !isset($_GET['taxonomy']) || 
            $_GET['taxonomy'] != 'category') {
        // bail early
        return $match;
    }
    $term_id = $_GET['tag_ID'];
    $selected_term = $rule['value'];
    if ($rule['operator'] == '==') {
        $match = ($selected_term == $term_id);
    } elseif ($rule['operator'] == '!=') {
        $match = ($selected_term != $term_id);
    }
    return $match;
}


function make_taxonomy_friendly_id($queried_object) {
    $query_id = ''; //a blank id will default to current post id
    if(is_category() || is_tag() || is_tax()) { //because taxonomy pages need special id's in the ACF Loop
        $query_id = $queried_object -> taxonomy . '_' . $queried_object -> term_id;
    }

    return $query_id;
}

//TIME STAMP
function bm_human_time_diff_enhanced( $duration = 1 ) {
    date_default_timezone_set("America/New_York");
    $post_time = get_the_time('U');
    $human_time = '';
    $yesterday = strtotime("yesterday");
    $today = strtotime("today");

    $time_now = date('U');

    // use human time if less that $duration days ago (60 days by default)
    // 60 seconds * 60 minutes * 24 hours * $duration days
    if ($post_time < $today && $post_time > $yesterday){
        $human_time = get_the_date('M j, Y');
    } elseif ( $post_time > $time_now - ( 60 * 60 * 24 * $duration ) ) {
        $human_time = human_time_diff( $post_time, current_time( 'timestamp', 1 )) . ' ago';
    } else {
        $human_time = get_the_date('M j, Y');
    }

    return $human_time;

}

// RETURNS INTERNAL OR EXTERNAL LINK APPROPRIATELY
function get_proper_link($post_id) {

    if(get_field('external_article', $post_id)) {
        $post_link_and_target = 'href="' . get_field('external_article_link', $post_id) . '" target="_blank"';
    } else {
        $post_link_and_target = 'href="' . get_permalink($post_id) . '"';
    }

    return $post_link_and_target;
}

function fb_move_admin_bar() {
    echo '
    <style type="text/css">
    body.customize-support {
        margin-top: -32px;
    }
    body.admin-bar #wphead {
       padding-top: 0;
    }
    body.admin-bar #footer {
       padding-bottom: 28px;
    }
    #wpadminbar {
        top: auto !important;
        bottom: 0;
    }
    #wpadminbar .quicklinks .menupop .ab-sub-wrapper {
        bottom: 28px;
    }
    @media screen and (max-width: 782px) {
        #wpadminbar {
            position:fixed;
        }

        body.customize-support {
            margin-top: -46px;
        }
    }
    </style>';
}
// on backend area
add_action( 'admin_head', 'fb_move_admin_bar' );
// on frontend area
add_action( 'wp_head', 'fb_move_admin_bar' );


// CUSTOMIZE NEWSLETTER RSS2 FEED
remove_all_actions( 'do_feed_rss2' );
add_action( 'do_feed_rss2', 'newsletter_feed_rss2', 10, 1 );

function newsletter_feed_rss2() {
    $rss2_template = get_template_directory() . '/feeds/feed-rss2.php';
    if(file_exists( $rss2_template )) {
        load_template( $rss2_template );
    }
}

function fields_in_feed($content) {
    if(is_feed()) {
        $post_id = get_the_ID();
        $output = '<p>' . get_field('article_preview_excerpt', $post_id) . '</p>';
        $content = $content.$output;
    }
    return $content;
}
add_filter('the_content','fields_in_feed');

// Move Yoast to bottom
function yoasttobottom() {
    return 'low';
}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

// include ad related functionality
include(__DIR__ .'/functions-ads.php');

add_action( 'embed_head', 'embed_top_style' );  
function embed_top_style(){ ?>
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7621114/7967552/css/fonts.css" />
    <style>
        .wp-embed {
            padding:0;
            border:0;
            box-shadow: none;
            -webkit-box-shadow:none;
        }

        .wp-embed-footer {
            display:none;
        }

        p.wp-embed-heading {
            font-family: "Mercury Display A", "Mercury Display B", Georgia, Times, serif;
            font-style: normal;
            font-weight: 700;
            word-wrap: break-word;
            font-size: 22px;
            line-height: 28px;
            letter-spacing: -0.05px;
            color: #1a1a1a;
            text-decoration: none;
        }

        .wp-embed-featured-image {
            width:100%;
            margin-bottom: 0;
        }

        @media(min-width:500px) {
            .wp-embed-featured-image {
                float:right;
                margin-left:20px;
                width:270px;
            }
        }
    </style>    
<?php }

// ADD ENDPOINTS TO SERVE HEADER AND FOOTER INDIVIDUALLY
function add_header_endpoint() {
    add_rewrite_endpoint( 'header-template', EP_ROOT );
}
add_action( 'init', 'add_header_endpoint' );

function header_template_redirect() {
    global $wp_query;
 
    if ( ! isset( $wp_query->query_vars['header-template'] ) ) {
        return;
    }
 
    include dirname( __FILE__ ) . '/header-template.php';
    exit;
}
add_action( 'template_redirect', 'header_template_redirect' );


function add_footer_endpoint() {
    add_rewrite_endpoint( 'footer-template', EP_ROOT );
}
add_action( 'init', 'add_footer_endpoint' );

function footer_template_redirect() {
    global $wp_query;
 
    if ( ! isset( $wp_query->query_vars['footer-template'] ) ) {
        return;
    }
 
    include dirname( __FILE__ ) . '/footer-template.php';
    exit;
}
add_action( 'template_redirect', 'footer_template_redirect' );