<?php

?>

<div class="sign-up-top">
	<h3 class="sign-up-title"><?php the_field('sign_up_title', 'options'); ?></h3>
	<img class="sign-up-image" src="<?php echo get_field('sign_up_image', 'options')[sizes][thumbnail]; ?>">
</div>

<p class="sign-up-body">
	<?php the_field('sign_up_body_copy', 'options'); ?>
</p>
<div id="newsletter-signup">
  <a class="gw-btn-black">Sign up now</a>
</div>