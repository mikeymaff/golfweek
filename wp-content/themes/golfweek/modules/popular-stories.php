<h2>Popular Now</h2>
<?php 
	if(!isset($popular_stories_limit)) {
		$popular_stories_limit = 7;
	}

	if ( function_exists('wpp_get_mostpopular')) {
	
		wpp_get_mostpopular(array(
			'post_type'      => "post,video_post,gallery,tracker",
			'stats_views'    => 0,
			'limit'          => $popular_stories_limit,
			'post_html'      => "<li><article><h3>{title}</h3></article></li>"
		));

	}
?>