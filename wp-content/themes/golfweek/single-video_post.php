<?php

get_header(); ?>
<style>
	.gw-ad.leaderboard {
		background-color: #191919;
	}
</style>

<div class="single-video-container">
	<main class="single-video_post">
		<div class="video-title-container">
			<h1><?php the_title(); ?></h1>

			<ul class="video-share-button-container">
				<li class="facebook-share" data-url="<?php echo get_permalink(); ?>"><a class="link-facebook"><i class="fa fa-facebook-square"></i>Share</a></li>
				<li id="twitter-wjs"><a href="https://twitter.com/intent/tweet?text=<?php echo the_title() . '&url=' . get_permalink(); ?>" target="_blank" class="link-twitter"><i class="fa fa-twitter"></i>Tweet</a></li>
				<li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_title() . ' ' . get_permalink(); ?>" class="link-email"><i class="fa fa-envelope"></i>Email</a></li>
			</ul>
		</div>

		<div class="video-and-modules-container">

			<div class="video-with-info">
				<div class="video-aspect-ratio-fix">
					<section class="video-container">
						<?php the_field('video_iframe_code'); ?>
					</section>
				</div>

				<div class="video-subhead-and-description">
					<h2><?php the_field('sub_headline'); ?></h2>
					<div class="description">
						<?php the_field('description'); ?>
					</div>
				</div>
			</div>


			<section class="right-modules">
				<?php include(locate_template('modules/ad.php')); ?>
			</section>

		</div>


	</main>
</div>

<?php get_footer(); ?>