	<?php $current_post_type = get_queried_object() -> post_type; 
	if($current_post_type != 'gallery' && $current_post_type != 'video_post' && !is_404()) { ?>

		<?php echo gw_ads_get_dfp_html('leaderboard', GW_ADS_SLOT_LB_BOTTOM, '970x90'); ?>
		
	<?php } ?>


	<footer>
		<div class="footer-container">
			<div class="footer-top-container">
				<div class="footer-top">
					<img src="<?php echo get_field('footer_brand_logo', 'options')[sizes][thumbnail]; ?>">
					<div class="footer-copyright">
						<?php the_field('footer_copyright', 'options'); ?>
					</div>
					<div class="footer-top-right">


						<img src="<?php echo get_field('footer_partner_logo', 'options')[sizes][thumbnail]; ?>">
						<div><?php the_field('footer_partner_text', 'options'); ?></div>

					</div>

				</div>
			</div>

			<div class="footer-bottom-container">
				<nav class="footer-nav">
					<?php wp_nav_menu( array(
						'menu' => 'Footer Nav'
					) ); ?>

					<div class="footer-dev-credit">
						<a href="http://www.wearebnr.com/" target="_blank">
							<img src="<?php echo get_template_directory_uri() . '/images/bnr-logo-footer.jpg' ?>">
							<span>Site by <br>Born & Raised</span>
						</a>
					</div>
				</nav>

				<div class="footer-social-and-apps">
					<hr>
					<div class="footer-social">
						<span>
							<?php the_field('social_text', 'options'); ?>
						</span>
						<?php if( get_field('facebook_url', 'options') ): ?>
							<a href="<?php the_field('facebook_url', 'options'); ?>"><i class="footer-fa fa fa-facebook"></i></a>
						<?php endif; ?>
						<?php if( get_field('twitter_url', 'options') ): ?>
							<a href="<?php the_field('twitter_url', 'options'); ?>"><i class="footer-fa fa fa-twitter"></i></a>
						<?php endif; ?>
						<?php if( get_field('instagram_url', 'options') ): ?>
							<a href="<?php the_field('instagram_url', 'options'); ?>"><i class="footer-fa fa fa-instagram"></i></a>
						<?php endif; ?>
						<?php if( get_field('youtube_url', 'options') ): ?>
							<a href="<?php the_field('youtube_url', 'options'); ?>"><i class="footer-fa fa fa-youtube-play"></i></a>
						<?php endif; ?>
						<?php if( get_field('linkedin_url', 'options') ): ?>
							<a href="<?php the_field('linkedin_url', 'options'); ?>"><i class="footer-fa fa fa-linkedin"></i></a>
						<?php endif; ?>
					</div>


				</div>

			</div>
		</div>
	</footer>


</div>

<div id="gw-modal"></div>
<div id="gw-modal-template-ad" class="gw-modal-template">
	<?php include(locate_template('partials/modal-ad.php')); ?>
</div>


<script type="text/javascript">
	window.GOLFWEEK_PATH = '<?php echo gw_ads_get_mapped_path(); ?>';
</script>

<!-- GOOGLE ANALYTICS -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-565433-1', 'auto');
	ga('send', 'pageview');
</script>

<?php wp_footer(); ?>


</body>
</html>