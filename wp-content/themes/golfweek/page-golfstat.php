<?php
	get_header();
?>

<main class="page-golfstat layout-content-with-modules">
	<?php include(locate_template('templates/content-with-modules.php')); ?>
</main>

<?php
	get_footer();
?>