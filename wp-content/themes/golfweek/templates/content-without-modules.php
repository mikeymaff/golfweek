<section class="content-and-title-container">
	<div class="content-without-modules-title-container">
		<h1 class="content-without-modules-title"><?php echo the_title(); ?></h1>
	</div>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="content-container">
				<?php the_content(); ?>

			</div>
		<?php endwhile; ?>
	<?php endif; ?>		
</section>

