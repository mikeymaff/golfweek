<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width">
  <meta name="google-site-verification" content="XrNFQYbQF5UN0n4X3acuaA9UPdmSK9KxPJRtWvr4lqU" />
  <?php wp_head(); ?>
  <title><?php the_title(); ?></title>
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7621114/7967552/css/fonts.css" />

  <script type='text/javascript'>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    (function() {
      var gads = document.createElement('script');
      gads.async = true;
      gads.type = 'text/javascript';
      var useSSL = 'https:' == document.location.protocol;
      gads.src = (useSSL ? 'https:' : 'http:') +
      '//www.googletagservices.com/tag/js/gpt.js';
      var node = document.getElementsByTagName('script')[0];
      node.parentNode.insertBefore(gads, node);
    })();
  </script>
</head>
<body>
  <?php echo gw_ads_get_dfp_html('skin', GW_ADS_SLOT_SKIN, '2200x1100'); ?>

  <div id="gw-modal-gallery">
    <div class="modal-inner">
      <div class="close-button-container">
        <i class="icon-golfweek-icons_close"></i>
      </div>
      <div class="modal-content">
        
      </div>
    </div>
  </div>
  
  <div id="content">
    <header class="site-header">

      <div class="search-container">
        <div class="input-container">
          <span class="icon-golfweek-icons_searchglass"></span>
          <span class="icon-golfweek-icons_close"></span>
          <form action="/search" method="GET">
            <input type="text" name="q" placeholder="Type your search" id="site-search-box">
          </form>
        </div>
      </div>

      <div class="header-row-top">

        <!-- Mobile only -->
        <div class="nav-toggle mobile">
          <span class="mobile-close icon-golfweek-icons_close"></span>
          <span class="mobile-burger icon-golfweek-icons_mobilemenu"></span>
        </div>

        <!-- logo should only be <h1> on homepage -->
        <h1><a href="/"><span class="icon-golfweek-icons_Golfweeklogo"></span></a></h1>

        <!-- Desktop only -->
        <div class="subscribe-search">
          <a href="https://ssl.palmcoastd.com/044EA/apps/2015DES?ikey=M**NAV">
            <div class="subscribe-text">
              <span class="subscribe-text-title">Subscribe to Golfweek</span>
              <span class="subscribe-text-price"><?php the_field('subscribe_line_2', 'options'); ?></span>
            </div>
            <img class="subscribe-image" src="<?php echo get_field('cover_image', 'options')[url]; ?>" alt="">
          </a>
          <div class="search-toggle expanded">
            <span class="icon-golfweek-icons_searchglass"></span>
          </div>
        </div>
      </div>

      <div class="header-row-bottom">
        <div class="inner">

          <!-- Condensed only -->
          <div class="title condensed"><a href="/"><span class="icon-golfweek-icons_Golfweeklogo"></a></div>

          <nav>

            <!-- Mobile only -->
            <div class="search-bar mobile">
              <form action="/search" method="GET">
                <input type="text" name="q" placeholder="Search Golfweek" >
              </form>
              <span class="icon-golfweek-icons_searchglass"></span>
            </div>

            <?php wp_nav_menu( array(
                'menu' => 'Main Nav'
            ) ); ?>

            <!-- Mobile only -->
            <div class="subscribe-search mobile">
              <a href="https://ssl.palmcoastd.com/044EA/apps/2015DES?ikey=M**NAV">
                <div class="subscribe-text">
                  <span class="subscribe-text-title">Subscribe to Golfweek</span>
                  <span class="subscribe-text-price"><?php the_field('subscribe_line_2', 'options'); ?></span>
                </div>
                <div class="subscribe-image">
                  <img src="<?php echo get_field('cover_image', 'options')[url]; ?>" alt="">
                </div>
              </a>
            </div>

          </nav>

          <ul class="social expanded">
            <?php if( get_field('facebook_url', 'options') ): ?>
              <li><a href="<?php the_field('facebook_url', 'options'); ?>"><span class="fa fa-facebook"></span></a></li>
            <?php endif; ?>
            <?php if( get_field('twitter_url', 'options') ): ?>
              <li><a href="<?php the_field('twitter_url', 'options'); ?>"><span class="fa fa-twitter"></span></a></li>
            <?php endif; ?>
            <li><a class="social-more" href="/social-links">More <span class="fa fa-angle-right"></span></a></li>
          </ul>

          <!-- Condensed only -->
          <ul class="social condensed">
            <li><a class="subscribe" href="https://ssl.palmcoastd.com/044EA/apps/2015DES?ikey=M**NAV">Subscribe <span class="fa fa-angle-right"></span></a></li>
            <li><a class="social" href="/social-links">Social <span class="fa fa-angle-right"></span></a></li>
            <li class="search-toggle"><span class="icon-golfweek-icons_searchglass"></span></li>
          </ul>

        </div>
      </div>

    </header>
    <div class="nav-curtain">
    </div>

    <?php if(!is_404()) {
      echo gw_ads_get_dfp_html('leaderboard', GW_ADS_SLOT_LB, '970x90');
    } ?>