<?php

get_header(); ?>
<main class="page-quick-shots">
	<div class="quick-shots-and-modules">
		<section class="quick-shots-title-and-article-list-container">

			<h1 class="quick-shots-title">
				<span class="quick">QUICK</span>
				<span class="shots">SHOTS</span>
			</h1>
			<div class="quick-shots-title-border"></div>

			<div class="quick-shots-article-list-container">
				<?php if(have_rows('featured_quick_shots')) { ?>
					<ul>
						<?php $list_index = 1;
						while(have_rows('featured_quick_shots')) : the_row();
							$quick_shot = get_sub_field('quick_shot');
							$post = $quick_shot;
							setup_postdata($post); ?>
							<li>
								<span class="position"><?php echo sprintf("%02d", $list_index); ?></span>

								<article class="article-content" id="<?php echo $post -> post_name; ?>">

									<?php if (has_post_thumbnail()) { ?>
										<figure><?php the_post_thumbnail('medium'); ?></figure>
									<?php }  ?>

									<h3><?php the_title(); ?></h3>

									<div class="quick-shots-content">
										<time><?php echo bm_human_time_diff_enhanced(); ?></time>
										<?php the_content(); ?>
									</div>

									<ul class="quick-shots-social-links">
										<li class="facebook-share" data-url="http://<?php echo $_SERVER['SERVER_NAME'] . get_quickshot_url($post); ?>"><a><i class="fa fa-facebook-square"></i><span class="social-name"> Share</span></a></li>
										<li id="twitter-wjs"><a href="https://twitter.com/intent/tweet?text=<?php echo the_title() . '&url=' . urlencode('http://' . $_SERVER['SERVER_NAME'] . get_quickshot_url($post)); ?>" target="_blank"><i class="fa fa-twitter"></i><span class="social-name"> Tweet</span></a></li>
										<li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_title() . ' ' . urlencode('http://' . $_SERVER['SERVER_NAME'] . get_quickshot_url($post)); ?>"><i class="fa fa-envelope"></i><span class="social-name"> Email</span></a></li>
									</ul>

								</article>
							</li>

							<?php $list_index++; ?>
							<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
					</ul>
				<?php } ?>
			</div>
		</section>

		<section class="right-modules">
			<?php include(locate_template('partials/module-builder.php')); ?>
		</section>

	</div>
</main>

<?php get_footer(); ?>