<div class="standard-article-list">
	<?php if($the_query->have_posts()) {
		while ($the_query->have_posts()) : $the_query->the_post(); 
			$post_link_and_target = get_proper_link(get_the_ID()); ?>

			<article>
				<?php if (has_post_thumbnail() && get_field('display_image_preview') ){ ?>
					<figure>
						<a <?php echo $post_link_and_target; ?>>
							<?php the_post_thumbnail('medium'); ?>
						</a>
					</figure>
				<?php } elseif (get_post_type() == 'video_post') { ?>
					<figure>
						<a <?php echo $post_link_and_target; ?>>
							<?php the_post_thumbnail('medium'); ?>
							<div class="media-btn"><i class="icon-golfweek-icons_play"></i></div>	
						</a>	
					</figure>	

				<?php }  
				elseif (get_post_type() == 'gallery') { ?>
					<figure>
						<a <?php echo $post_link_and_target; ?>>
							<?php the_post_thumbnail('medium'); ?>
							<div class="media-btn-gallery">
								<i class="fa icon-golfweek-icons_gallery"></i>
								<span><?php echo count(get_field('gallery'));?></span>
							</div>
						</a>
					</figure>	
				<?php }?>

				<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
				<time><?php echo bm_human_time_diff_enhanced(); ?></time>
				<p class="summary"> <?php echo get_field('article_preview_excerpt') ?></p>
			</article>
		<?php endwhile; ?>
		<?php wp_reset_postdata(); 
	} ?>
</div>