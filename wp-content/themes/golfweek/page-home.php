<?php
	get_header();

	$homeID = get_queried_object() -> ID;
	$posts_to_exclude_in_latest = array();

?>

<main class="page-home">
	<div class="hero-container">
		<section class="feature-and-banner-container">
			<?php if(get_field('featured_banner')){ ?>
				<div class="featured-banner">
					<?php if(get_field('featured_banner_label')){ ?>
						<span class="featured-banner-label"><?php the_field('featured_banner_label'); ?></span>
					<?php } ?>
					<?php if(get_field('featured_banner_copy')){ ?>
						<span class="featured-banner-copy"><?php the_field('featured_banner_copy'); ?></span>
					<?php } ?>
					<?php if(get_field('featured_banner_image')){ ?>
						<img src="<?php echo get_field('featured_banner_image')[sizes][thumbnail]; ?>"/>
					<?php } ?>
				</div>
			<?php } else {?>

			<?php } ?>
			<div class="main-and-sub-feature-container">
				<div class="main-feature-container">
					<?php $main_featured_story = get_field('main_featured_story');
					if($main_featured_story) {
						$post = $main_featured_story;
						setup_postdata($post);
						$post_link_and_target = get_proper_link(get_the_ID()); ?>

						<article>
							<?php if (has_post_thumbnail()) { ?>
								<figure>
									<a <?php echo $post_link_and_target; ?>>
										<?php if(get_field('featured_banner', $homeID) || get_field('show_featured_related_links', $homeID)) { ?>
											<?php the_post_thumbnail('medium'); ?>
										<?php } else { ?>
											<?php the_post_thumbnail('medium'); ?>
										<?php } ?>
									</a>
								</figure>
							<?php }  ?>
							<?php if(get_field('feature_flag', $homeID) && !get_field('featured_banner', $homeID)) { ?>
								<div class="featured-flag">
									<?php the_field('feature_flag_copy', $homeID); ?>
								</div>
							<?php } ?>
							<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
							<div class="time-and-byline">
								<time><?php echo bm_human_time_diff_enhanced(); ?></time>
								-
								<span class="byline">
									<?php $staff = get_the_terms(get_the_ID(), 'staff');
									if(count($staff) > 0) { ?>
										by
										<?php $staff_index = 0;
										foreach ($staff as $person) {
											echo '<a href="/staff/' . $person -> slug . '">' . $person -> name . '</a>';
											$staff_index++;
											if($staff_index < count($staff)) {
												echo ', ';
											}
										}
									} ?>
								</span>
							</div>

							<p class="summary">
								<?php the_field('article_preview_excerpt'); ?>
							</p>
							<?php array_push($posts_to_exclude_in_latest, get_the_ID()); ?>
							<?php wp_reset_postdata(); ?>

							<?php if(get_field('show_featured_related_links') && !get_field('featured_banner')) { ?>
								<div class="related-links">
									<h4><?php the_field('featured_related_links_title'); ?></h4>
									<?php if(have_rows('featured_related_links')){ ?>
										<ul>
											<?php while(have_rows('featured_related_links')) : the_row(); ?>
												<li>
													<?php $my_post_id = get_sub_field('link_to_article') -> ID;
													$post_link_and_target = get_proper_link($my_post_id); ?>
													<article>
														<h3>
															<a <?php echo $post_link_and_target; ?>><?php echo get_the_title($my_post_id); ?></a>
														</h3>
													</article>
												</li>
												<?php array_push($posts_to_exclude_in_latest, $my_post_id); ?>
											<?php endwhile; ?>
										</ul>
									<?php } ?>
								</div>
							<?php } ?>
						</article>
					<?php } ?>
				</div>
				<div class="sub-features-container">
					<?php if(get_field('show_sub_feature_banner') && !get_field('featured_banner')) { ?>
						<div class="sub-feature-banner">
							<img src="<?php echo get_field('sub_feature_banner')[url]; ?>">
						</div>
					<?php } ?>
					<?php if(have_rows('sub_featured_stories')){ ?>
						<?php while(have_rows('sub_featured_stories')): the_row();
							$sub_featured_story = get_sub_field('story');
							$post = $sub_featured_story;
							setup_postdata($post);
							$post_link_and_target = get_proper_link(get_the_ID()); ?>

							<article>
								<?php if (has_post_thumbnail() && get_field('display_image_preview')) { ?>
									<figure>
										<a <?php echo $post_link_and_target; ?>>
											<?php the_post_thumbnail('thumbnail'); ?>
										</a>
									</figure>
								<?php }  ?>
								<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
								<time><?php echo bm_human_time_diff_enhanced(); ?></time>
							</article>

							<?php array_push($posts_to_exclude_in_latest, get_the_ID()); ?>
							<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
					<?php } ?>
				</div>
			</div>
		</section>
		<section class="popular-and-ad-container">
			<div class="module popular-stories-module">
				<?php $popular_stories_limit = 7; ?>
				<?php include(locate_template('modules/popular-stories.php')); ?>
				<?php unset($popular_stories_limit); ?>
			</div>
			<div class="module ad-module">
				<?php include(locate_template('modules/ad.php')); ?>
			</div>
		</section>
	</div>

	<div class="mid-content-container">
		<div class="quick-shots-and-latest-news">
			<section class="quick-shots">
				<h2>
					<a href="/quick-shots">
						<span class="quick">QUICK</span>
						<span class="shots">SHOTS</span>
					</a>
				</h2>
				<?php $quick_shots_page_id = get_page_by_path('quick-shots') -> ID;
				if(have_rows('featured_quick_shots', $quick_shots_page_id)) { ?>
					<ul>
						<?php $list_index = 1;
						while(have_rows('featured_quick_shots', $quick_shots_page_id)) : the_row();
							$quick_shot = get_sub_field('quick_shot');
							$post = $quick_shot;
							setup_postdata($post); ?>
							<li>
								<span class="position"><?php echo sprintf("%02d", $list_index); ?></span>
								<article>
									<?php if (has_post_thumbnail()) { ?>
										<figure>
											<a href="<?php echo get_quickshot_url($post); ?>">
												<?php the_post_thumbnail('thumbnail'); ?>
											</a>
										</figure>
									<?php }  ?>
									<h3><a href="<?php echo get_quickshot_url($post); ?>"><?php the_title(); ?></a></h3>
								</article>
							</li>
							<?php $list_index++; ?>
							<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
					</ul>
				<?php } ?>
			</section>
			<section class="latest-news">
				<div class="section-title-container">
					<h2>
						<a href="/latest-news/page/2" class="latest-news-link">
							Latest News
						</a>
					</h2>
					<a href="/latest-news/page/2"><i class="fa fa-angle-double-right"></i></a>
				</div>
				<?php $args = array(
					'post_status'    => 'publish',
					'post_type'		 => array('post', 'video_post', 'gallery', 'tracker'),
					'post__not_in' => $posts_to_exclude_in_latest
				);
				$the_query = new WP_Query($args); ?>
				<?php include(locate_template('partials/standard-article-list.php')); ?>
				<div class="more-latest-container">
					<a href="/latest-news/page/2">MORE LATEST <i class="fa fa-angle-double-right"></i></a>
				</div>
			</section>
		</div>
		<section class="mid-modules">
			<?php $home_page_modules = 'module_importer_home_center';
			include(locate_template('partials/module-builder.php'));
			unset($home_page_modules); ?>
		</section>
	</div>

	<div class="latest-videos-and-photos-container">
		<div class="latest-media-inner">
			<section class="latest-media">
				<div class="title-and-more-container">
					<h2><i class="icon-golfweek-icons_videos"></i> <span>LATEST VIDEOS</span></h2>
					<div class="more-latest-container">
						<a href="/videos"><span>MORE VIDEOS </span><i class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				<?php $recent_video_posts = wp_get_recent_posts(array('numberposts' => 4, 'post_status' => 'publish', 'post_type' => 'video_post'), OBJECT);
				foreach ($recent_video_posts as $recent_video_post) {
					$post = $recent_video_post;
					setup_postdata($post); ?>

					<article>
						<?php if (has_post_thumbnail()) { ?>
							<figure>
								<a <?php echo get_proper_link(get_the_ID()); ?>>
									<?php the_post_thumbnail('thumbnail'); ?>
									<div class="video-play-btn"><i class="fa icon-golfweek-icons_play"></i></div>
								</a>
							</figure>

						<?php }  ?>
						<h3><a <?php echo get_proper_link(get_the_ID()); ?>><?php the_title(); ?></a></h3>
						<time><?php echo bm_human_time_diff_enhanced(); ?></time>
					</article>

					<?php wp_reset_postdata(); ?>
				<?php }?>
			</section>
			<section class="latest-media">
				<div class="title-and-more-container">
					<h2><i class="icon-golfweek-icons_photos"></i>LATEST PHOTOS</h2>
					<div class="more-latest-container">
						<a href="/photos"><span>MORE PHOTOS </span><i class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				<?php $recent_galleries = wp_get_recent_posts(array('numberposts' => 4, 'post_status' => 'publish', 'post_type' => 'gallery'), OBJECT);
				foreach ($recent_galleries as $recent_gallery) {
					$post = $recent_gallery;
					setup_postdata($post); ?>

					<article>
						<?php if (has_post_thumbnail()) { ?>
							<figure>
								<a <?php echo get_proper_link(get_the_ID()); ?>>
									<?php the_post_thumbnail('thumbnail'); ?>
									<div class="gallery-play-btn"><i class="fa icon-golfweek-icons_gallery"></i> <span><?php echo count(get_field('gallery'));?></span></div>
								</a>
							</figure>
						<?php }  ?>
						<h3><a <?php echo get_proper_link(get_the_ID()); ?>><?php the_title(); ?></a></h3>
						<time><?php echo bm_human_time_diff_enhanced(); ?></time>
					</article>

					<?php wp_reset_postdata(); ?>
				<?php }?>
			</section>
		</div>
	</div>

	<div class="lower-content-container">
		<div class="featured-categories-and-modules-container">
			<div class="featured-categories-container">
				<?php if(have_rows('featured_categories')) {
					while(have_rows('featured_categories')) : the_row();
					$featured_category = get_sub_field('featured_category'); ?>
						<section class="featured-category">
							<div class="section-title-container">
								<h2>
									<a class="cat-header-link" href="/<?php echo $featured_category -> slug; ?>">
										<?php echo $featured_category -> name; ?>
									</a>
								</h2>
								<a class="chevron-link" href="/<?php echo $featured_category -> slug; ?>">
									<i class="fa fa-angle-double-right"></i>
								</a>
							</div>
							<?php $recent_posts_in_category = wp_get_recent_posts(array(
								'numberposts' => 5,
								'post_status' => 'publish',
								'category'    => $featured_category -> term_id,
								'post_type'   => array('post', 'video_post', 'gallery', 'tracker')
								), OBJECT);
							$post_loop_index = 0;
							foreach ($recent_posts_in_category as $recent_post_in_category) {
								$post = $recent_post_in_category;
								setup_postdata($post);
								$post_link_and_target = get_proper_link(get_the_ID()); ?>
								<?php if($post_loop_index == 0) { ?>
									<div class="featured-post-in-category">
										<article>
											<?php if (has_post_thumbnail()) { ?>
												<figure>
													<a <?php echo $post_link_and_target; ?>>
														<?php the_post_thumbnail('medium'); ?>
													</a>
												</figure>
											<?php }  ?>
											<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
											<div class="time-and-byline">
												<time><?php echo bm_human_time_diff_enhanced(); ?></time>
												<span class="byline">
													<?php $staff = wp_get_post_terms(get_the_ID(), 'staff');
													if(count($staff) > 0) { ?>
														by
														<?php $staff_index = 0;
														foreach ($staff as $person) {
															echo '<a href="/staff/' . $person -> slug . '">' . $person -> name . '</a>';
															$staff_index++;
															if($staff_index < count($staff)) {
																echo ', ';
															}
														}
													} ?>
												</span>
											</div>
										</article>
									</div>
								<?php } elseif($post_loop_index == 1) { ?>
									<div class="sub-feature-posts-in-category">
										<article><h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3></article>
								<?php } else { ?>
										<article><h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3></article>
								<?php } ?>
								<?php wp_reset_postdata();
								$post_loop_index++; ?>
							<?php } ?>
							<?php if($post_loop_index > 0) { ?>
								</div> <?php //This closes out the sub-feature-posts div ?>
							<?php } ?>
						</section>
					<?php endwhile;
				} ?>
			</div>

			<section class="lower-modules">
				<?php $home_page_modules = 'module_importer_home_bottom';
				include(locate_template('partials/module-builder.php'));
				unset($home_page_modules); ?>
			</section>
		</div>

		<div class="sub-featured-categories-container">
			<div class="sub-featured-categories-container-inner">
				<?php $all_sub_featured_categories = array(get_field('sub_featured_category_1'), get_field('sub_featured_category_2'), get_field('sub_featured_category_3'), get_field('sub_featured_category_4'));
				foreach ($all_sub_featured_categories as $sub_featured_category) { ?>
					<section class="sub-featured-category">
						<div class="section-title-container">
							<h2>
								<a class="cat-header-link" href="<?php echo get_category_link( $sub_featured_category -> term_id ); ?>">
									<?php echo $sub_featured_category -> name; ?>
								</a>	
							</h2>

							<a class="chevron-link" href="<?php echo get_category_link( $sub_featured_category -> term_id ); ?>">
								<i class="fa fa-angle-double-right"></i>
							</a>
						</div>
						<?php $recent_posts_in_category = wp_get_recent_posts(array(
							'numberposts' => 4, 
							'post_status' => 'publish', 
							'category' => $sub_featured_category -> term_id,
							'post_type'   => array('post', 'video_post', 'gallery', 'tracker')
						), OBJECT);

						foreach ($recent_posts_in_category as $recent_post_in_category) {
							$post = $recent_post_in_category;
							setup_postdata($post);
							$post_link_and_target = get_proper_link(get_the_ID()); ?>

							<article>
								<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
							</article>

							<?php wp_reset_postdata(); ?>
						<?php } ?>
					</section>
				<?php }?>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>