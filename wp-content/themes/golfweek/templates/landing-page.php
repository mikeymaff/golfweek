<?php
	$queried_object = get_queried_object();
	$query_id = make_taxonomy_friendly_id($queried_object);
	$posts_to_exclude_in_latest = array();

	$selected_post_types = get_field('post_type_selector', $query_id);
	if(count($selected_post_types) > 0) {
		$content_type = $selected_post_types;
	} elseif(!isset($content_type)) {
		$content_type = 'post';
	}
?>
<div class="landing-page-container">
	<section class="featured-articles">

		<?php 
		//This area can feature up to 7 articles
		//Anthing less than 7 articles will be combined with the most recent articles
		//The below code uses this logic to build an array of all the
		//the posts that will be looped through to build out the while featured area
		$total_featured_articles = 7;
		$featured_posts = array();
		
		if(have_rows('featured_articles', $query_id)) {
		    while (have_rows('featured_articles', $query_id)) : the_row();
				array_push($featured_posts, get_sub_field('article'));
				array_push($posts_to_exclude_in_latest, get_sub_field('article') -> ID);
	        endwhile;
	    }

	    $remaining_amount_of_posts = $total_featured_articles - count($featured_posts);

	    if($remaining_amount_of_posts > 0) {
	    	if($is_taxonomy) {
				$args = array(
					'tax_query' => array(
						array(
							'taxonomy' => $queried_object -> taxonomy,
							'field'    => 'id',
							'terms'    => $queried_object -> term_id,
						),
					),
					'post_type'      => $content_type,
					'post_status'    => 'publish',
					'posts_per_page' => $remaining_amount_of_posts,
					'paged'          => get_query_var('paged'),
					'post__not_in' => $posts_to_exclude_in_latest
				);
			} elseif($is_tag) {
				$args = array(
					'tag'            => $queried_object -> slug,
					'post_type'      => $content_type,
					'post_status'    => 'publish',
					'posts_per_page' => $remaining_amount_of_posts,
					'paged'          => get_query_var('paged'),
					'post__not_in' => $posts_to_exclude_in_latest
				);
			} else {
				$args = array(
					'cat'            => $queried_object -> term_id,
					'post_type'      => $content_type,
					'post_status'    => 'publish',
					'posts_per_page' => $remaining_amount_of_posts,
					'paged'          => get_query_var('paged'),
					'post__not_in' => $posts_to_exclude_in_latest
				);
			}
		    $the_query = new WP_Query($args);
		    if($the_query->have_posts()) {
		    	while ($the_query->have_posts()) : $the_query->the_post();
		    		array_push($featured_posts, get_post());
		    		array_push($posts_to_exclude_in_latest, get_the_ID());
		    	endwhile;
		    } 
		} ?>

	    <?php $loop_index = 0;
	    foreach ($featured_posts as $featured_post) {
			$post = $featured_post;
			setup_postdata($post); 
			$post_link_and_target = get_proper_link(get_the_ID()); ?>

			<?php if($loop_index == 0) { ?>
				<div class="big-and-small-features">
					<div class="big-feature">
						<div class="category-title-container">
							<h1 class="category-title"><?php echo $queried_object -> name; ?></h1>
						</div>
						<article>
							<?php if (has_post_thumbnail()) { ?>
								<figure>
									<a <?php echo $post_link_and_target; ?>>
										<?php the_post_thumbnail('medium'); ?>
									</a>
								</figure>
							<?php }  ?>
							<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
							<time><?php echo bm_human_time_diff_enhanced(); ?></time>
							<p class="summary">
								<?php the_field('article_preview_excerpt'); ?>
							</p>
						</article>
					</div>
				<?php } elseif($loop_index <= 4) { ?>
					<?php if($loop_index == 1) { ?>
						<div class="small-features">
					<?php } ?>
					<article>
						<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
						<time><?php echo bm_human_time_diff_enhanced(); ?></time>
					</article>
					<?php if($loop_index == 4) { ?>
						</div>
					<?php } ?>
				<?php } else { ?>
					<?php if($loop_index == 5) { ?>
				</div>
					<div class="medium-features">
				<?php } ?>
				<article>
				
					<?php if (get_post_type() == 'post' || get_post_type() == 'tracker' ){ ?>
						<figure>
							<a <?php echo $post_link_and_target; ?>>
								<?php the_post_thumbnail('medium'); ?>
							</a>
						</figure>
					<?php } elseif (get_post_type() == 'video_post') { ?>
						<figure>
							<a <?php echo $post_link_and_target; ?>>
								<?php the_post_thumbnail('medium'); ?>
								<div class="media-btn"><i class="icon-golfweek-icons_play"></i></div>	
							</a>	
						</figure>	

					<?php }  elseif (get_post_type() == 'gallery') { ?>
						<figure>
							<a <?php echo $post_link_and_target; ?>>
								<?php the_post_thumbnail('medium'); ?>
								<div class="media-btn-gallery">
									<i class="fa icon-golfweek-icons_gallery"></i>
									<span><?php echo count(get_field('gallery'));?></span>
								</div>
							</a>
						</figure>	
					<?php }?>

					<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
					<time><?php echo bm_human_time_diff_enhanced(); ?></time>
				</article>
				<?php if($loop_index == 6) { ?>
					</div>
				<?php } ?>
			<?php } ?>
		
			<?php $loop_index++;
			wp_reset_postdata(); ?>
		<?php }?>
	</section>
	<div class="promoted-sections-article-list-and-sidebar-container">
		<?php if (have_rows('promoted_sections', $query_id)): ?>
			<section class="promoted-sections-container">
				<hr class='promoted-sections-hr'>
				<?php if(have_rows('promoted_sections', $query_id)) {
				    while (have_rows('promoted_sections', $query_id)) : the_row(); ?>
						<?php if(get_row_layout() == 'promoted_category') { ?>
							<div class="promoted-section">
								<div class="promoted-section-title-container">
									
									<h2>
										<a class="cat-header-link" href="<?php echo get_category_link(get_sub_field('category') -> term_id); ?>">
											<?php echo get_sub_field('category') -> name ; ?>
										</a>
									</h2>

									<a class="chevron-link" href="<?php echo get_category_link(get_sub_field('category') -> term_id); ?>">
										<i class="fa fa-angle-double-right"></i>
									</a>
								</div>
								<?php $args = array(
									'cat'            => get_sub_field('category') -> term_id,
									'post_type'      => $content_type,
									'post_status'    => 'publish',
									'posts_per_page' => get_sub_field('number_of_items'),
								);
								$the_query = new WP_Query($args);
								if($the_query->have_posts()) {
									while ($the_query->have_posts()) : $the_query->the_post();
										$post_link_and_target = get_proper_link(get_the_ID()); ?>
										<article>
											<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
										</article>
									<?php endwhile; ?>
								<?php } ?>
							</div>
						<?php } elseif (get_row_layout() == 'promoted_tag') { ?>
							<div class="promoted-section">
								<div class="promoted-section-title-container">
									<h2>
										<a class="cat-header-link" href="<?php echo get_tag_link(get_sub_field('tag') -> term_id); ?>">
											<?php echo get_sub_field('tag') -> name; ?>
										</a>
									</h2>
									<a class="chevron-link" href="<?php echo get_tag_link(get_sub_field('tag') -> term_id); ?>">
										<i class="fa fa-angle-double-right"></i>
									</a>
								</div>
								<?php $args = array(
									'tag'            => get_sub_field('tag') -> slug,
									'post_type'      => $content_type,
									'post_status'    => 'publish',
									'posts_per_page' => get_sub_field('number_of_items'),
								);
								$the_query = new WP_Query($args);
								if($the_query->have_posts()) {
									while ($the_query->have_posts()) : $the_query->the_post();
										$post_link_and_target = get_proper_link(get_the_ID()); ?>
										<article>
											<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
										</article>
									<?php endwhile; ?>
								<?php } ?>
							</div>
						<?php } ?>

					<?php endwhile;
				} ?>
			</section>
		<?php endif ?>

		<div class="article-and-right-module-container">
			<?php if (have_rows('promoted_sections', $query_id)): ?>
			<section class="article-list article-list-narrow">
			<?php else: ?>
			<section class="article-list article-list-wide">
			<?php endif ?>
				<?php if($is_taxonomy) {
					$args = array(
						'tax_query' => array(
							array(
								'taxonomy' => $queried_object -> taxonomy,
								'field'    => 'id',
								'terms'    => $queried_object -> term_id,
							),
						),
						'post_type'      => $content_type,
						'post_status'    => 'publish',
						'paged'          => get_query_var('paged'),
						'post__not_in' => $posts_to_exclude_in_latest
					);
				} elseif($is_tag) {
					$args = array(
						'tag'            => $queried_object -> slug,
						'post_type'      => $content_type,
						'post_status'    => 'publish',
						'paged'          => get_query_var('paged'),
						'post__not_in' => $posts_to_exclude_in_latest
					);
				} else {
					$args = array(
						'cat'            => $queried_object -> term_id,
						'post_type'      => $content_type,
						'post_status'    => 'publish',
						'paged'          => get_query_var('paged'),
						'post__not_in' => $posts_to_exclude_in_latest
					);
				}
				$the_query = new WP_Query($args); ?>
				<div class="article-list-title-container">


					<h2>
						<?php echo get_next_posts_link('Latest News', $the_query->max_num_pages ); ?>
					</h2>
					

					<?php echo get_next_posts_link(' <i class="fa fa-angle-double-right"></i>', $the_query->max_num_pages ); ?>
				
				</div>
				<?php include(locate_template('partials/standard-article-list.php'));
				echo '<div class="article-next-btn">';
				echo get_next_posts_link(('MORE ' . $queried_object -> name) . '  <i class="fa fa-angle-double-right"></i>', $the_query->max_num_pages );
				echo '</div>';
				?>
			</section>
			<section class="right-modules">
				<?php include(locate_template('partials/module-builder.php')); ?>
			</section>
		</div>
	</div>
</div>
