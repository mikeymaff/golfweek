<?php get_header(); 

?>


<?php if ( is_404() ): ?>
	<script>
	  var is404Page = true;
	</script>
<?php endif ?>

<main class="page-404">

	
	<div class="container-404"style="background-image: url(<?php echo get_field('background_image_404', 'options')[sizes][large]; ?>);">
		<div class="curtain">
		</div>
		<div class="content-container-404">
			<h1 class="title-404">Sand Trap.</h1>
			<h3 class="top-headline-404">The page you're looking for isn't here.</h3>
			<h3 class="bottom-headline-404">Looking for an older post?</h3>
			<a href="http://archive.golfweek.com<?php echo $_SERVER["REQUEST_URI"] ; ?>" target='_blank' class="gw-btn-white"> Try the archive</a>
		</div>
	</div>

</main>

<?php get_footer(); ?>