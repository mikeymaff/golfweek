<?php 
	get_header(); 

	$queried_object = get_queried_object();
	$is_subcategory = $queried_object -> parent;

	$term_string = $queried_object -> taxonomy . '_' . $queried_object -> term_id;
	$page_layout = get_field('page_layout', $term_string);

	$is_tag = true;

	// The page layout option does not get set upon category creation
	// So we have to implement the default logic here, if page layout
	// has no value
	// ALL PAGES AFTER PAGE 1 GET AN ARTICLE LIST LAYOUT
	if(!$page_layout) {
		$page_layout = 'article-list';
	}

	if(get_query_var('paged') > 1) {
		$page_layout = 'article-list';
	} elseif(!$page_layout) {
		if($is_subcategory) {
			$page_layout = 'article-list';
		} else {
			$page_layout = 'landing-page';
		}
	}
?>
<main class="tag list tag-page tag-<?php echo $queried_object -> slug; ?> <?php echo $page_layout ?>">
	<?php if($page_layout == 'article-list') { 
		include(locate_template('templates/article-list.php'));
	} elseif($page_layout == 'landing-page') {
		include(locate_template('templates/landing-page.php'));
	} elseif ($page_layout == 'layout-media') {
		include(locate_template('templates/media.php'));
	}?>
</main>
	
<?php get_footer(); ?>