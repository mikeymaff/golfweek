<?php 
	get_header(); 
?>
<?php
	$queried_object = get_queried_object();
	$query_id = make_taxonomy_friendly_id($queried_object);
?>
<main class="page-social-links">
	<div class="category-title-container">
		<h1 class="category-title"><?php echo $queried_object -> post_title; ?></h1>
	</div>

	<div class="social-container">
		<div class="social-row-1">

			<div class="social-facebook">
				<h3 class="social-headline"><span>Facebook</span>  <i class="social-links-fa fa fa-facebook"></i></h3>
			
				<?php if( have_rows('facebook') ):
					while ( have_rows('facebook') ) : the_row(); ?>
							<li>
							 	<a href="<?php echo get_sub_field('facebook_link'); ?>"target="_blank">
							 		<?php echo get_sub_field('facebook_text'); ?>
							 	</a>
							</li> 
						 <?php 
				    endwhile;
				endif; ?>
			</div>
			<hr>
			<div class="social-twitter">
				<h3 class="social-headline"><span>Twitter</span> <i class="social-links-fa fa fa-twitter"></i></h3>
			
				<?php if( have_rows('twitter') ):
				    while ( have_rows('twitter') ) : the_row(); ?>
						<li>
						 	<a href="<?php echo get_sub_field('twitter_link'); ?>"target="_blank">
						 		@<?php echo get_sub_field('twitter_text'); ?>
						 	</a>
						</li> 
					 <?php 
				    endwhile;
				endif; ?>
			</div>
		</div>
		<hr>
		 <div class="social-row-2">
			<div class="social-staff">
				<h3 class="social-headline"><span>Golfweek Staff</span>  <i class="social-links-fa fa fa-twitter"></i></h3>
			
				<?php
					//list terms in a given taxonomy
					$taxonomy = 'staff';
					$term_args=array(
					  'hide_empty' => false,
					  'orderby' => 'name',
					  'order' => 'ASC'
					);
					$tax_terms = get_terms($taxonomy,$term_args);
				?>

				<ul>
					<?php

					foreach ($tax_terms as $tax_term) {
						$term_string = 'staff_' . $tax_term->term_id;
						
						?>
						<?php if (get_field('golfweek_staff_member', $term_string)): ?>	
						<?php if (get_field('twitter_handle', $term_string)): ?>
				
						<li> 				
							<a class="social-staff-name-link" href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)) ?>"> 
								<?php echo $tax_term->name; ?>
							</a>	
							<a class= "social-staff-twitter-handle" href="https://twitter.com/<?php echo get_field('twitter_handle', $term_string) ?>"target="_blank"> 
								@<?php echo get_field('twitter_handle', $term_string); ?>
							</a>

								<?php else: ?>
								<li>
									<a class="social-staff-name-link" style="margin-bottom: 18px;" href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)) ?>"> 
										<?php echo $tax_term->name; ?>
									</a>	
								</li>
								<?php endif ?>
						</li>
						<?php endif ?>	
					<?php 	
					}
					?>
				</ul>
			</div> 
		</div>
	</div>
</main>

<?php get_footer(); ?>