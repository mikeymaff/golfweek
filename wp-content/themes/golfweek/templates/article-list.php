<?php
$queried_object = get_queried_object();
$query_id = make_taxonomy_friendly_id($queried_object);

$selected_post_types = get_field('post_type_selector', $query_id);
if(count($selected_post_types) > 0) {
	$content_type = $selected_post_types;
} else {
	if (!isset($content_type)){
		$content_type = 'post';
	}
}

?>
<div class="container-for-latest">
	<div class="title-and-article-list-container">
		<div class="category-title-container">
			<h1 class="category-title">
				<?php if ( $queried_object -> name){
					echo $queried_object -> name;

				} else {
					echo $queried_object -> post_title;
				} ?>

			</h1>
		</div>

		<section class="article-list">
			<?php 
			if ($content_type == 'gallery' || $content_type == 'video_post') {
				$args = array(
					'post_status'    => 'publish',
					'post_type'      => $content_type,
					'paged'          => get_query_var('paged'),
				);
			} elseif(isset($is_taxonomy)) {
				$args = array(
					'tax_query' => array(
						array(
							'taxonomy' => $queried_object -> taxonomy,
							'field'    => 'id',
							'terms'    => $queried_object -> term_id,
						),
					),
					'post_status'    => 'publish',
					'post_type'      => $content_type,
					'paged'          => get_query_var('paged'),
				);
			} elseif(isset($is_tag)) {
				$args = array(
					'tag'            => $queried_object -> slug,
					'post_status'    => 'publish',
					'post_type'      => $content_type,
					'paged'          => get_query_var('paged')
				);
			} else {
				$args = array(
					'cat'            => $queried_object -> term_id,
					'post_status'    => 'publish',
					'post_type'      => $content_type,
					'paged'          => get_query_var('paged'),
				);
			}
			$the_query = new WP_Query($args);
			include(locate_template('partials/standard-article-list.php'));
			echo ' <div class="prev-next-btn-container"> <span class="article-prev-btn">' . get_previous_posts_link( '<i class="fa fa-angle-double-left"></i> PREVIOUS PAGE' ) . '</span>';
			
			
			if (get_previous_posts_link() && get_next_posts_link('', $the_query->max_num_pages )) {
				echo '<div class="line"></div>';
			}
			echo '<span class="article-next-btn">' . get_next_posts_link( 'NEXT PAGE <i class="fa fa-angle-double-right"></i>', $the_query->max_num_pages ) . '</span></div>'; ?>
		</section>
	</div>

	<section class="right-modules">
		<?php include(locate_template('partials/module-builder.php')); ?>
	</section>
</div>	