<?php

get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css">
<style>
	.gw-ad.leaderboard {
		background-color: #191919;
	}
</style>
<script src="<?php echo get_template_directory_uri(); ?>/js/swiper.jquery.min.js"></script>
<?php include(locate_template('partials/gallery.php')); ?>
<script>

	jQuery(document).ready(function(){
		GOLFWEEK.GALLERY.init({
			isGalleryDetailPage:true
		});
	});
</script>
<?php get_footer(); ?>