<?php
	$queried_object = get_queried_object();
	$query_id = make_taxonomy_friendly_id($queried_object);
	$posts_to_exclude_in_latest = array();
	$page_title = get_the_title();
	if(is_tax() || is_tag() || is_category()) {
		$page_title = $queried_object -> name;
	}

	$selected_post_types = get_field('post_type_selector', $query_id);
	if(count($selected_post_types) > 0) {
		$content_type = $selected_post_types;
	} elseif(!isset($content_type)) {
		$content_type = 'post';
	}


?>
<section class="featured-media">

	<?php 
	//This area can feature up to 3 articles
	//Anthing less than 3 articles will be combined with the most recent articles
	//The below code uses this logic to build an array of all the
	//the posts that will be looped through to build out the while featured area
	$total_featured_media = 3;
	$featured_posts = array();
	
	if(have_rows('featured_media', $query_id)) {
	    while (have_rows('featured_media', $query_id)) : the_row();
			array_push($featured_posts, get_sub_field('media'));
			array_push($posts_to_exclude_in_latest, get_sub_field('media') -> ID);
        endwhile;
    }
    $remaining_amount_of_posts = $total_featured_media - count($featured_posts);

    if($remaining_amount_of_posts > 0) {
    	if(isset($is_taxonomy)) {
			$args = array(
				'tax_query' => array(
					array(
						'taxonomy' => $queried_object -> taxonomy,
						'field'    => 'id',
						'terms'    => $queried_object -> term_id,
					),
				),
				'post_type'      => $content_type,
				'post_status'    => 'publish',
				'posts_per_page' => $remaining_amount_of_posts,
				'paged'          => get_query_var('paged'),
				'post__not_in' => $posts_to_exclude_in_latest
			);
		} elseif(isset($is_tag)) {
			$args = array(
				'tag'            => $queried_object -> slug,
				'post_type'      => $content_type,
				'post_status'    => 'publish',
				'posts_per_page' => $remaining_amount_of_posts,
				'paged'          => get_query_var('paged'),
				'post__not_in' => $posts_to_exclude_in_latest
			);
		} else {
			$args = array(
				'cat'            => $queried_object -> term_id,
				'post_type'      => $content_type,
				'post_status'    => 'publish',
				'posts_per_page' => $remaining_amount_of_posts,
				'paged'          => get_query_var('paged'),
				'post__not_in' => $posts_to_exclude_in_latest
			);
		}
	    $the_query = new WP_Query($args);
	    if($the_query->have_posts()) {
	    	while ($the_query->have_posts()) : $the_query->the_post();
	    		array_push($featured_posts, get_post());
	    		array_push($posts_to_exclude_in_latest, get_the_ID());
	    	endwhile;
	    } 
	} ?>

    <?php $loop_index = 0;
    
    foreach ($featured_posts as $featured_post) {
		$post = $featured_post;
		setup_postdata($post); ?>

		<?php if($loop_index == 0) { ?>
			<div class="big-feature">
				<div class="category-title-container">
					<h1 class="category-title"><?php echo $page_title; ?></h1>
				</div>
				<article>
					<?php if (has_post_thumbnail()) { ?>
						<a <?php echo get_proper_link(get_the_ID()); ?>>
							<figure>
								<?php the_post_thumbnail('large'); ?>
								<div class="big-feature-categories">
									<h4 class="title-category"><?php echo get_the_category()[0] -> name  ; ?></h4>
									<h3><?php the_title(); ?></h3>
								</div>
								<div class="curtain">
									<?php if($content_type == 'video_post') { ?>
										<div class="media-btn"><i class="icon-golfweek-icons_play"></i></div>
									<?php } elseif ($content_type == 'gallery') { ?>
										<div class="media-btn-gallery">
											<i class="fa icon-golfweek-icons_gallery"></i>
											<span><?php echo count(get_field('gallery'));?></span>
										</div>
									<?php } ?>
								</div>
							</figure>
						</a>	
					<?php }  ?>
				</article>
			</div>
		<?php } elseif($loop_index <= 2) { ?>
			<?php if($loop_index == 1) { ?>
				<div class="medium-features">
			<?php } ?>
			<article>
				<a <?php echo get_proper_link(get_the_ID()); ?>>
					<?php if (has_post_thumbnail()) { ?>
						<figure>
							<?php the_post_thumbnail('thumbnail'); ?>
							<?php if($content_type == 'video_post') { ?>
								<div class="media-btn"><i class="icon-golfweek-icons_play"></i></div>
							<?php } elseif ($content_type == 'gallery') { ?>
								<div class="media-btn-gallery">
									<i class="fa icon-golfweek-icons_gallery"></i>
									<span><?php echo count(get_field('gallery'));?></span>
								</div>
							<?php } ?>
						</figure>
					<?php }  ?>
				</a>
				<h3><a <?php echo get_proper_link(get_the_ID()); ?>><?php the_title(); ?></a></h3>
				<time><?php echo bm_human_time_diff_enhanced(); ?></time>
			</article>
			<?php if($loop_index == 2) { ?>
				</div>
			<?php } ?>
		<?php } ?>
		
		<?php $loop_index++;
		wp_reset_postdata(); ?>
	<?php }?>
</section>

<div class="bottom-content-container">
	<div class="latest-and-featured-container">
		<?php if(get_field('show_latest_media_module', $query_id)) { ?>
			<section class="latest-media media-module">
				<?php if(isset($is_taxonomy)) {
					$args = array(
						'tax_query' => array(
							array(
								'taxonomy' => $queried_object -> taxonomy,
								'field'    => 'id',
								'terms'    => $queried_object -> term_id,
							),
						),
						'post_type'      => $content_type,
						'post_status'    => 'publish',
						'paged'          => get_query_var('paged'),
						'post__not_in'   => $posts_to_exclude_in_latest,
						'posts_per_page'  => 15
					);
				} elseif(isset($is_tag)) {
					$args = array(
						'tag'            => $queried_object -> slug,
						'post_type'      => $content_type,
						'post_status'    => 'publish',
						'paged'          => get_query_var('paged'),
						'post__not_in'   => $posts_to_exclude_in_latest,
						'posts_per_page'  => 15
					);
				} else {
					$args = array(
						'cat'            => $queried_object -> term_id,
						'post_type'      => $content_type,
						'post_status'    => 'publish',
						'paged'          => get_query_var('paged'),
						'post__not_in'   => $posts_to_exclude_in_latest,
						'posts_per_page'  => 15
					);
				}

				$the_query = new WP_Query($args); ?>
				<div class="media-title-container">
					<h2><?php echo get_next_posts_link(get_field('latest_media_title', $query_id), $the_query->max_num_pages ); ?></h2>
					<?php echo get_next_posts_link(' <i class="fa fa-angle-double-right"></i>', $the_query->max_num_pages ); ?>
				</div>

				<?php include(locate_template('partials/media-module.php')); ?>
			</section>
		<?php } ?>

		<?php if(have_rows('promoted_sections', $query_id)) {
		    while (have_rows('promoted_sections', $query_id)) : the_row(); ?>
				<?php if(get_row_layout() == 'promoted_category') { ?>
					<section class="media-module">
						<div class="media-title-container">
							<h2>
								<a class="cat-header-link" href="<?php echo get_category_link(get_sub_field('category') -> term_id); ?>">
								<?php echo get_sub_field('category') -> name ; ?>
								</a>
							</h2>
							
							<a class="chevron-link" href="<?php echo get_category_link(get_sub_field('category') -> term_id); ?>">
								<i class="fa fa-angle-double-right"></i>
							</a>
						</div>
						<?php $args = array(
							'cat'            => get_sub_field('category') -> term_id,
							'post_type'      => $content_type,
							'post_status'    => 'publish',
							'posts_per_page' => 6,
						);
						$the_query = new WP_Query($args);
 						?>
						<?php include(locate_template('partials/media-module.php')); ?>
					</section>
				<?php } elseif (get_row_layout() == 'promoted_tag') { ?>
					<section class="media-module">
						<div class="media-title-container">
							<h2>
								<a class="cat-header-link" href="<?php echo get_tag_link(get_sub_field('tag') -> term_id); ?>">
									<?php echo get_sub_field('tag') -> name; ?>
								</a>
							</h2>

							<a class="chevron-link" href="<?php echo get_tag_link(get_sub_field('tag') -> term_id); ?>">
								<i class="fa fa-angle-double-right"></i>
							</a>
						</div>
						<?php $args = array(
							'tag'            => get_sub_field('tag') -> slug,
							'post_type'      => $content_type,
							'post_status'    => 'publish',
							'posts_per_page' => 6,
						);
						$the_query = new WP_Query($args);
 						?>
						<?php include(locate_template('partials/media-module.php')); ?>
					</section>
				<?php } ?>
				<?php endwhile;
			} ?>

	</div>
	<section class="right-modules">
		<?php include(locate_template('partials/module-builder.php')); ?>
	</section>
</div>