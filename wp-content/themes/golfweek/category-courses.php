<?php 
	get_header(); 

	$content_type = 'post';

	if(!$page_layout) {
		$page_layout = 'media';
	}

	if(get_query_var('paged') > 1) {
		$page_layout = 'article-list';
	} elseif(!$page_layout) {
		if($is_subcategory) {
			$page_layout = 'article-list';
		} 
	}
?>

<main class="page-courses list-courses layout-media">
	<?php if($page_layout == 'media') { 
		include(locate_template('templates/media.php'));
	} elseif($page_layout == 'article-list') { 
		include(locate_template('templates/article-list.php'));
	}?>
</main>

<?php get_footer(); ?>