<?php

?>

<div class="single-gallery-container">
	<main class="single-gallery">
		<div class="gallery-title-container">
			<h1><?php the_title(); ?></h1>

			<ul class="gallery-share-button-container">
				<li class="facebook-share" data-url="<?php echo get_permalink(); ?>"><a class="link-facebook"><i class="fa fa-facebook-square"></i>Share</a></li>
				<li id="twitter-wjs"><a href="https://twitter.com/intent/tweet?text=<?php echo the_title() . '&url=' . get_permalink(); ?>" target="_blank" class="link-twitter"><i class="fa fa-twitter"></i>Tweet</a></li>
				<li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_title() . ' ' . get_permalink(); ?>" class="link-email"><i class="fa fa-envelope"></i>Email</a></li>
			</ul>
		</div>

		<div class="gallery-and-modules-container">
			<section class="gallery-container">
				<?php $gallery = get_field('gallery');
				if(count($gallery) > 0) { ?>
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<?php foreach ($gallery as $image) { ?>
								<div class="swiper-slide">
									<figure>
										<img src="<?php echo $image[sizes][large]; ?>" alt="<?php echo $image[alt]; ?>">
									</figure>
								</div>
							<?php } ?>
						</div>
					</div>
					<div class="swiper-btn-cap-pag-container">
						<div class="swiper-pagination pagination-desktop"></div>
						<div class="figcaption-container">
							<?php foreach ($gallery as $image) { ?>
								<figcaption>
									<?php echo $image[caption]; ?>
								</figcaption>
							<?php } ?>
						</div>
						<div class="swiper-button-next"><i class="fa fa-angle-double-right"></i></div>
				        <div class="swiper-button-prev"><i class="fa fa-angle-double-left"></i></div>
						<div class="swiper-pagination pagination-mobile"></div>
					</div>
				<?php } ?>
				<div class="gallery-subhead-and-description">
					<h2><?php the_field('sub_headline'); ?></h2>
					<div class="description">
						<?php the_field('description'); ?>
					</div>
				</div>
			</section>
			<section class="right-modules">
				<?php include(locate_template('modules/ad.php')); ?>
			</section>
		</div>

		
	</main>
</div>