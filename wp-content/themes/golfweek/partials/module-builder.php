<?php

$queried_object = get_queried_object();
$query_id = make_taxonomy_friendly_id($queried_object);
$module_importer_string = 'module_importer';

// if this page has no modules set, use the global ones form the options page
if(is_front_page()) {
	$module_importer_string = $home_page_modules;
} elseif(!have_rows($module_importer_string, $query_id)) {
	$query_id = 'options';
}

if(have_rows($module_importer_string, $query_id)) {
    while (have_rows($module_importer_string, $query_id)) : the_row(); ?>

        <?php if(get_row_layout() == 'popular_stories_module') { ?>
			<div class="module popular-stories-module">
				<?php include(locate_template('modules/popular-stories.php')); ?>
			</div>
        <?php } elseif(get_row_layout() == 'ad_module'){ ?>
			<div class="module ad-module">
				<?php include(locate_template('modules/ad.php')); ?>
			</div>
        <?php } elseif(get_row_layout() == 'promo_box_module'){
        	$promo_box = get_sub_field('promo_box_select'); ?>
        	<div class="module promo-box-module">
        		<?php include(locate_template('modules/promo-box.php')); ?>
        	</div>
        <?php } elseif(get_row_layout() == 'subscribe_module'){ ?>
			<div class="module subscribe-module" style="background-image:url(<?php echo get_field('subscribe_image', 'options')[url]; ?>);">
				<?php include(locate_template('modules/subscribe.php')); ?>
			</div>
        <?php } elseif(get_row_layout() == 'sign_up_now_module'){ ?>
			<div class="module sign-up-module">
				<?php include(locate_template('modules/sign-up.php')); ?>
			</div>
        <?php } ?>

    <?php endwhile;
} ?>