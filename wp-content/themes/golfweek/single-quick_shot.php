<?php

get_header(); ?>
<main class="single-quick_shot">
	<div class="quick-shots-detail-page">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <section class="quick-shots-detail-article">
        <div class="quick-shots-detail-header">
          <h1 class="quick-shots-title">
            <span class="quick">QUICK</span>
            <span class="shots">SHOTS</span>
          </h1>
          <div class="quick-shots-title-border"></div>
        </div>
        <article class="article-content">
          <?php if (has_post_thumbnail()) { ?>
            <figure><?php the_post_thumbnail('medium'); ?></figure>
          <?php }  ?>

          <h3><?php the_title(); ?></h3>

          <div class="quick-shots-content">
            <time><?php echo bm_human_time_diff_enhanced(); ?></time>
            <?php the_content(); ?>
          </div>

          <ul class="quick-shots-social-links">
            <li class="facebook-share" data-url="<?php echo get_permalink(); ?>"><a><i class="fa fa-facebook-square"></i><span class="social-name"> Share</span></a></li>
            <li id="twitter-wjs"><a href="https://twitter.com/intent/tweet?text=<?php echo the_title() . '&url=' . get_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i><span class="social-name"> Tweet</span></a></li>
            <li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_title() . ' ' . get_permalink(); ?>"><i class="fa fa-envelope"></i><span class="social-name"> Email</span></a></li>
          </ul>
        </article>

        <div class="more-quick-shots-container">
          <a href="/quick-shots">MORE QUICK SHOTS <i class="fa fa-angle-double-right"></i></a>
        </div>
      </section>
      
    <?php endwhile; endif; ?>

    <section class="right-modules">
      <?php include(locate_template('partials/module-builder.php')); ?>
    </section>
  </div>
</main>

<?php get_footer(); ?>