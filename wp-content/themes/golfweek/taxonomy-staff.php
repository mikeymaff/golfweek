<?php 
	get_header(); 

	$queried_object = get_queried_object();
	$query_id = make_taxonomy_friendly_id($queried_object);
?>

<main class="taxonomy list taxonomy-staff">
	<section class="staff-all-content-container">
		<div class="staff-info-page-container">
			<?php if (get_field('bio_picture', $query_id)): ?>
				<div class="staff-image-container" style="background-image: url(<?php echo get_field('bio_picture', $query_id)[sizes][medium];?>)">
					<!-- <img class="" src=""> -->
				</div>
			<?php endif ?>
			<?php if (get_field('bio_picture', $query_id) || get_field('instagram_handle', $query_id) || get_field('twitter_handle', $query_id)
			|| get_field('email', $query_id) || get_field('title', $query_id) || get_field('career_highlights', $query_id)): ?>

				<div class="staff-headers-and-contact-links-container">
					<div class="staff-headers">
						<?php if (get_field('golfweek_staff_member', $query_id)): ?>
							<h2 class="staff-title-header">Golfweek Staff</h2>
						<?php endif ?>
							<h1 class="staff-name-header"><?php echo get_queried_object() -> name; ?></h1>
						
					</div>
					<hr class="staff-hr-1">
					<ul class="staff-contact-link-container">
						<?php if( get_field('twitter_handle', $query_id) ): ?>
							<li class"staff-contact-links">
								<a href="https://twitter.com/<?php echo get_field('twitter_handle', $query_id);?>" target="_blank">
								<i class="staff-fa fa fa-twitter"></i>  @<?php echo get_field('twitter_handle', $query_id);?>
								</a>
							</li>
						<?php endif; ?>
						<?php if( get_field('instagram_handle', $query_id) ): ?>
							<li class"staff-contact-links">
								<a href="https://instagram.com/<?php echo get_field('instagram_handle', $query_id);?>" target="_blank">
								<i class="staff-fa fa fa-instagram"></i>  <?php echo get_field('instagram_handle', $query_id);?>
								</a>
							</li>
						<?php endif; ?>
						<?php if( get_field('email', $query_id) ): ?>
							<li class"staff-contact-links">
								<a href="mailto:<?php echo get_field('email', $query_id);?>">
								<i class="staff-fa fa fa-envelope"></i>  <?php echo get_field('email', $query_id);?>
								</a>
							</li>
						<?php endif; ?>
					</ul>

				</div>
				
				<div class="staff-info-container">
					<?php if( get_field('title', $query_id) ): ?>
						<div class="staff-title">
							<h3>Title: </h3>
							<p><?php echo get_field('title', $query_id);?></p>	
						</div>
					<?php endif ?>
					<?php if( get_field('hometown', $query_id) ): ?>
						<div class="staff-hometown">
							<h3>Hometown: </h3>
							<p><?php echo get_field('hometown', $query_id);?></p>
						</div>
					<?php endif ?>
					<?php if( get_field('career_highlights', $query_id) ): ?>
						<div class="staff-career-highlights">
							<h3>Career highlights: </h3>
							<p><?php echo get_field('career_highlights', $query_id);?></p>
						</div>
					<?php endif ?>
				</div>
				<hr>
			<?php endif ?>

			<section class="staff-article-list-and-header-container">
				<div class="staff-article-header">
					<?php if (get_field('bio_picture', $query_id) || get_field('instagram_handle', $query_id) || get_field('twitter_handle', $query_id)
					|| get_field('email', $query_id) || get_field('title', $query_id) || get_field('career_highlights', $query_id)): ?>
						<h4 class="with-top-bottom-padding">Stories by <?php echo get_queried_object() -> name; ?></h4>
					<?php else: ?>
						<h4>Stories by <?php echo get_queried_object() -> name; ?></h4>
					<?php endif ?>
				</div>

				<div class="staff-article-list-container">
					<?php 
					echo get_field('term_id', $query_id);
						$args = array(
							'post_type' => array('post', 'video_post', 'gallery', 'tracker'),
							'tax_query' => array(
								array(
									'taxonomy' => 'staff',
									'field' => 'term_id',
									'terms'    => get_queried_object() -> term_id
								)
							),
							'post_status'    => 'publish',
							'paged'          => get_query_var('paged'),
						);
					
					$the_query = new WP_Query($args);
					include(locate_template('partials/standard-article-list.php'));
					
					echo ' <div class="prev-next-btn-container"> <span class="article-prev-btn">' . get_previous_posts_link( '<i class="fa fa-angle-double-left"></i> PREVIOUS PAGE' ) . '</span>';
					if (get_previous_posts_link() && get_next_posts_link()) {
						# code...
						echo '<div class="line"></div>';
					}
					echo '<span class="article-next-btn">' . get_next_posts_link( 'NEXT PAGE <i class="fa fa-angle-double-right"></i>', $the_query->max_num_pages ) . '</span></div>'; ?>
				</div>
			</section>	
		</div>
	

		<section class="right-modules">
			<?php include(locate_template('partials/module-builder.php')); ?>
		</section>
	</section>
</main>

<?php get_footer(); ?>