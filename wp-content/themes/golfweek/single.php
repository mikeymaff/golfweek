<?php
	$queried_object = get_queried_object();
	// $taxonomy = $queried_object->taxonomy;
	// $term_id = $queried_object->term_id;  
get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css">

<main class="single-post">
	<article>
		<div class="content">
			<header>
				<h4 class="category">
					<?php if(get_the_category()[0] -> parent) { ?>
						<a href="<?php echo get_category_link(get_the_category()[0] -> parent); ?>">
							<?php echo get_the_category_by_ID(get_the_category()[0] -> parent); ?>
						</a>
						/
					<?php } ?>
					<a href="<?php echo get_category_link(get_the_category()[0] -> term_id); ?>">
						<?php echo get_the_category()[0] -> name; ?>
					</a>
				</h4>
				<h1>
					<?php the_title(); ?>
				</h1>
				<div class="byline-and-date">
					<div class="author-credit">
						<?php
						// load all 'category' terms for the post
						     $term_list = wp_get_post_terms($post->ID, 'staff', array("fields" => "all"));                        
						     $i = 0;
						     $len = count($term_list); ?>

						     <?php if ($len > 0 ){ ?>
					            <?php foreach($term_list as $term_single) {
					              
						              $bio_pic = get_field('bio_picture', $term_single )[sizes][medium];
						              $twitter = get_field('twitter_handle', $term_single );
						              $instagram = get_field('instagram_handle', $term_single );
						              $email = get_field('email', $term_single ); ?>
						            
							            <?php if( get_field('bio_picture', $term_single) ): ?>
							            	<a class="name-link" href="/staff/<?php echo $term_single -> slug; ?>">
												<div class="author-bio-pic" style="background-image: url(<?php echo $bio_pic; ?>)"></div>
											</a>	
										<?php endif; ?>
										<div class="author-popover">
												<div class="author-credit-name">
													<span class="by">By </span> 
													<a class="name-link" href="/staff/<?php echo $term_single -> slug; ?>">
														<span class="name"><?php echo $term_single -> name; ?></span> 
													</a>
													<?php if ( get_field('twitter_handle', $term_single) || get_field('instagram_handle', $term_single) || get_field('email', $term_single) ): ?>	
														<i class="fa fa-angle-right"></i> 
													<?php endif ?>
												</div>

											<?php if ( get_field('twitter_handle', $term_single) || get_field('instagram_handle', $term_single) || get_field('email', $term_single) ): ?>	
												<ul class="author-contact-container">
													<a href="/staff/<?php echo $term_single -> slug; ?>">
														<li class="author-name">
															<?php echo $term_single -> name; ?> <i class="fa fa-angle-down"></i>
														</li>
													</a>
													<?php if( get_field('twitter_handle', $term_single) ): ?>
														<li class="author-contact-links">
															<a href="https://twitter.com/<?php echo $twitter ;?>" target="_blank">
															<i class="staff-fa fa fa-twitter"></i>  @<?php echo $twitter ;?>
															</a>
														</li>
													<?php endif; ?>
													
													<?php if( get_field('instagram_handle', $term_single) ): ?>
														<li class="author-contact-links">
															<a href="https://instagram.com/<?php echo $instagram ;?>" target="_blank">
															<i class="staff-fa fa fa-instagram"></i>  <?php echo $instagram ;?>
															</a>
														</li>
													<?php endif; ?>

													<?php if( get_field('email', $term_single) ): ?>
														<li class="author-contact-links">
															<a href="mailto:<?php echo $email ;?>">
															<i class="staff-fa fa fa-envelope"></i>  <?php echo $email ;?>
															</a>
														</li>
													<?php endif; ?>
												</ul>

											<?php endif ?>
										</div>
								
						        <?php }
					      	}
						?>
						<time><?php echo bm_human_time_diff_enhanced(); ?></time>
					</div>
				</div>
			</header>
			<div class="article-body">
				<ul class="share-article-top">
					<li class="facebook-share" data-url="<?php echo get_permalink(); ?>"><a class="link-facebook"><i class="fa fa-facebook-square"></i><span>Share</span></a></li>
					<li id="twitter-wjs"><a href="https://twitter.com/intent/tweet?text=<?php echo the_title() . '&url=' . get_permalink(); ?>" target="_blank" class="link-twitter"><i class="fa fa-twitter"></i><span>Tweet</span></a></li>
					<li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_title() . ' ' . get_permalink(); ?>" class="link-email"><i class="fa fa-envelope"></i><span>Email</span></a></li>
					<li><a onclick="window.print()" class="link-print"><i class="fa fa-print"></i><span>Print</span></a></li>
				</ul>
				<?php $is_archive = in_category('archive');
				if($is_archive) {
					$image_src = wp_get_attachment_url(get_post_thumbnail_id());
					$archive_image_exists = false;
					if(@getimagesize($image_src)) {
						$archive_image_exists = true;
					} 
				} ?>
				<?php if($is_archive): ?>
					<?php if($archive_image_exists): ?>
						<figure class="article-hero">
							<img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" />
						</figure>
					<?php endif ?>
				<?php elseif(has_post_thumbnail()): ?>
					<figure class="article-hero">
						<?php the_post_thumbnail('large'); ?>
						<figcaption>
							<?php if(get_field('hero_image_override_caption')) { ?>
								<span class="caption-text">
									<?php the_field('hero_image_caption'); ?>
								</span>
							<?php } elseif(get_post(get_post_thumbnail_id()) -> post_excerpt) { ?>
								<span class="caption-text">
									<?php echo get_post(get_post_thumbnail_id()) -> post_excerpt; ?>
								</span>
							<?php } ?>

							<?php if(get_field('source', get_post_thumbnail_id())) { ?>
								<span class="source">(<?php the_field('source', get_post_thumbnail_id()) ?>)</span>
							<?php } ?>
						</figcaption>
					</figure>
					<div class="article-notes"> <?php echo sanitize_text_field(get_field('article_notes')); ?></div>
				<?php endif  ?>

				<?php if(in_category('archive')): ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				<?php endif ?>

				<?php if( have_rows('content') ): ?>
					<?php while( have_rows('content') ): the_row(); ?>
<?php // BODY COPY ?>
							<?php if( get_row_layout() == 'body_copy' ): ?>
								<?php if (get_sub_field('add_drop_cap')): ?>
									<div class="dropcap"> <?php the_sub_field('body_copy'); ?></div>
								<?php else: ?>	
										<?php the_sub_field('body_copy'); ?>
								<?php endif ?>
								
<?php // SUB HEADLINE ?>
							<?php elseif(get_row_layout() == 'sub-headline' ): ?>

								<h2><?php the_sub_field('sub-headline'); ?></h2>
<?php // IMAGE ?>
							<?php elseif(get_row_layout() == 'image' ): ?>

								<figure>
									<img src="<?php echo get_sub_field('image')[sizes][large] ?>" alt="<?php echo get_sub_field('image')['alt'] ?>">
									<figcaption>
										<span class="caption-text"><?php echo get_sub_field('image')['caption'] ?></span> / <span class="source"><?php echo get_field('source', get_sub_field('image')[id]); ?></span>
									</figcaption>
								</figure>
<?php // PULL QUOTE ?>
							<?php elseif(get_row_layout() == 'pull_quote' ): ?>

							<?php if (get_sub_field('add_quotes')): ?>
								<blockquote class="pull-quote">
									<span class="quote"><?php the_sub_field('quote_text'); ?></span>
								</blockquote>
							<?php else: ?>	
								<blockquote class="pull-quote">
									<?php the_sub_field('quote_text'); ?>
								</blockquote>
							<?php endif ?>

<?php // AD ?>
							<?php elseif(get_row_layout() == 'ad' ): ?>
								<div class="ad-in-article">
									<?php echo gw_ads_get_dfp_html('article', GW_ADS_SLOT_ARTICLE, '300x250'); ?>
								</div>

<?php // RELATED COVERAGE ?>
							<?php elseif(get_row_layout() == 'related_coverage_box' ):
								$related_coverage = get_sub_field('related_coverage'); ?>
								
								<?php if($related_coverage) { ?>
									<aside class="related-coverage">
										<h4>Related Coverage</h4>
										<ul>
											<?php foreach ($related_coverage as $related_coverage_post) {
												$post = $related_coverage_post;
												setup_postdata($post); ?>

												<li>
													<article>
														<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
													</article>
												</li>

												<?php wp_reset_postdata(); ?>
											<?php }?>
										</ul>
									</aside>
								<?php } ?>
<?php // IMAGE GALLERY ?>
							<?php elseif(get_row_layout() == 'image_gallery' ):
								$image_gallery_post = get_sub_field('gallery_select');
								$post = $image_gallery_post;
								setup_postdata($post); ?>

								<article class="gallery-preview">
									<a href="<?php the_permalink(); ?>" data-gallery-id="<?php echo get_the_id(); ?>">
										<?php if (has_post_thumbnail()) { ?>
											<figure>
												<?php the_post_thumbnail('large'); ?>
												<div class="curtain">
												</div>
												<h3><?php the_title(); ?></h3>
												<div class="view-gallery-button">
													<span>View Gallery</span><i class="icon-golfweek-icons_gallery"></i> <?php echo count(get_field('gallery')); ?>
												</div>
											</figure>
										<?php }  ?>
									</a>
								</article>

								<?php wp_reset_postdata(); ?>
<?php // VIDEO ?>
							<?php elseif(get_row_layout() == 'video' ):
								$video_post = get_sub_field('video_select');
								$post = $video_post;
								setup_postdata($post); ?>

								<div class="video-container">
									<?php the_field('video_iframe_code'); ?>
								</div>

								<?php wp_reset_postdata(); ?>
							<?php endif; ?>

					<?php endwhile; ?>

				<?php endif; ?>

				<ul class="share-button-container">
					<li class="facebook-share" data-url="<?php echo get_permalink(); ?>"><a class="link-facebook"><i class="fa fa-facebook-square"></i>Share</a></li>
					<li id="twitter-wjs"><a href="https://twitter.com/intent/tweet?text=<?php echo the_title() . '&url=' . get_permalink(); ?>" target="_blank" class="link-twitter"><i class="fa fa-twitter"></i>Tweet</a></li>
					<li><a href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo the_title() . ' ' . get_permalink(); ?>" class="link-email"><i class="fa fa-envelope"></i>Email</a></li>
				</ul>

				<div class="related-topics">
					<h5>RELATED TOPICS:</h5>
					<?php $tags = wp_get_post_tags($queried_object -> ID);
					foreach ($tags as $tag) { ?>
						<a href="<?php echo get_tag_link($tag -> term_id); ?>"><?php echo $tag -> name; ?></a>
					<?php }
					$players = wp_get_post_terms($queried_object -> ID, 'player');
					foreach ($players as $player) { ?>
						<a href="<?php echo get_term_link($player -> term_id); ?>"><?php echo $player -> name; ?></a>
					<?php } ?>

				</div>

			</div>
			<section class="right-modules">
				<?php include(locate_template('partials/module-builder.php')); ?>
			</section>
		</div>

	</article>
</main>

<div class="more-from-this-category-container">
	<div class="more-from-this-category-inner">
		<?php $postCategory = get_the_category($queried_object -> ID); ?>
		<div class="section-title-container">
			<h5>
				<a href="<?php echo get_category_link($postCategory[0] -> term_id); ?>">
					More from <?php echo $postCategory[0] -> name; ?>
				</a>
				
			</h5>
			<a href="<?php echo get_category_link($postCategory[0] -> term_id); ?>"><i class="fa fa-angle-double-right"></i></a>
		</div>
		<?php $args = array(
			'cat'            => $postCategory[0] -> term_id,
			'post_status'    => 'publish',
			'posts_per_page' => 4,
			'post__not_in'   => array($queried_object -> ID)
		);
		$the_query = new WP_Query($args);
		if($the_query->have_posts()) {
			while ($the_query->have_posts()) : $the_query->the_post();
				$post_link_and_target = get_proper_link(get_the_ID()); ?>
				<article>
					<?php if (has_post_thumbnail()) { ?>
						<figure>
							<a <?php echo $post_link_and_target; ?>>
								<?php the_post_thumbnail('thumbnail'); ?>
							</a>
						</figure>
					<?php }  ?>
					<h3><a <?php echo $post_link_and_target; ?>><?php the_title(); ?></a></h3>
					<time><?php echo bm_human_time_diff_enhanced(); ?></time>
				</article>
			<?php endwhile; ?>
		<?php } ?>
	</div>
</div>

<div class="comments-outer-container">
	<div class="comments-container">
		<div class="comments-header">
			<h6>STORY COMMENTS</h6>
			<span class="show-hide-button"><span class="show-button">Show <i class="fa fa-chevron-down"></i></span><span class="hide-button">Hide <i class="fa fa-chevron-up"></i></span></span>
		</div>
		<div class="facebook-comments-container">
			<div class="fb-comments" data-href="<?php echo get_permalink($queried_object -> ID); ?>" data-numposts="10" data-width="100%"></div>
		</div>
	</div>
</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1646295555626359";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/swiper.jquery.min.js"></script>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<?php get_footer(); ?>