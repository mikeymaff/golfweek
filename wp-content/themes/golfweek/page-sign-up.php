
<?php wp_head(); ?>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<html>
<head>
	<title>Sign Up</title>
	<link rel="stylesheet/less" type="text/css" href="../styles.less" />
</head>
<body class="sign-up">
	<div class="sign-up-title-container">
		<h2 class="sign-up-title"><span class="icon-golfweek-icons_Golfweeklogo"></span></h2>
		<h3 class="sign-up-subtitle">Newsletters & E-Publications</h3>
		<h4 class="sign-up-subtext">Subscribe to Golfweek’s newsletters to receive  news and exclusive content</h4>
	</div>
	<form action="http://golfweek.us2.list-manage.com/subscribe/post?u=e8c5029f9dfb9d400e405c5c8&amp;id=a04fca388b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
	<div class="mc-field-group email-fields">
		<label class= "list-number" for="mce-EMAIL">1</label>
		<input type="email" value="" name="EMAIL" class="required-email" id="mce-EMAIL" placeholder="Enter your email address">
	</div>

	<div class="mc-field-group input-group">
		<label class="list-number">2</label>
		<h3>Select all the products you'd like to receive: </h3>
		<ul>
			<li>
				<div class="item-wrap">
					<!-- <div class="checkbox-mobile"> -->
						<input type="checkbox" checked="checked" value="128" name="group[13][128]" id="mce-group[13]-13-7"><label class="list-label" for="mce-group[13]-13-7">Toy Box Extra</label>
<!-- 						<label class="checkbox-mobile-label" for="checkbox-mobile"></label>
					</div> -->
					<i class="fa fa-question-circle" onclick="" aria-hidden="true">
						<div class="sign-up-popover" onclick="">
							<h4>Toy Box Extra</h4>
							<p>This gear-centric e-magazine delivers the latest golf equipment headlines and news every Wednesday.</p>
						</div>
					</i>
				</div>
			</li>
			<li class="right-col-list">
				<div class="item-wrap">
					<input checked="checked" type="checkbox" value="1" name="group[13][1]" id="mce-group[13]-13-0"><label class="list-label" for="mce-group[13]-13-0">Approach Shots</label>
					<i class="fa fa-question-circle" onclick="" aria-hidden="true">
						<div class="sign-up-popover" onclick="">
							<h4>Approach Shots</h4>
							<p>This daily update delivers the game's hottest headlines with a special emphasis on professional golf.</p>
						</div>
					</i>
				</div>
			</li>
		    <li>
		    	<div class="item-wrap">
			    	<input type="checkbox" checked="checked" value="1024" name="group[13][1024]" id="mce-group[13]-13-10"><label class="list-label" for="mce-group[13]-13-10">Ryder Cup + Majors Dailies</label>
			    	<i class="fa fa-question-circle" onclick="" aria-hidden="true">
				    	<div class="sign-up-popover" onclick="">
				    		<h4>Ryder Cup + Majors Dailies</h4>
				    		<p>With its new Ryder Cup and majors e-magazines, Golfweek equips fans with insider news and information leading up to, during and after these key events.</p>
				    	</div>
				    </i>
			    </div>
		    </li>
		    <li class="right-col-list">
		    	<div class="item-wrap">
			    	<input checked="checked" type="checkbox" value="16" name="group[13][16]" id="mce-group[13]-13-4"><label class="list-label" for="mce-group[13]-13-4">The Golf Life</label>
			    	<i class="fa fa-question-circle" onclick="" aria-hidden="true">
				    	<div class="sign-up-popover" onclick="">
				    		<h4>The Golf Life</h4>
				    		<p>This golf travel/architecture guide is sent twice per week and offers tips on the best places to stay and play.</p>
				    	</div>
				    </i>
			    </div>
		    </li>
		    <li>
		    	<div class="item-wrap">
			    	<input checked="checked" type="checkbox" value="2" name="group[13][2]" id="mce-group[13]-13-1"><label class="list-label" for="mce-group[13]-13-1">Junior Extra</label>
			    	<i class="fa fa-question-circle" onclick="" aria-hidden="true">
			    		<div class="sign-up-popover" onclick="">
			    			<h4>Junior Extra</h4>
			    			<p>This weekly report provides the exclusive, inside scoop on the next generation of rising golf stars.</p>
			    		</div>
			    	</i>
		    	</div>
		    </li>
		    <li class="right-col-list">
		    	<div class="item-wrap">
			    	<input checked="checked" type="checkbox" value="4096" name="group[13][4096]" id="mce-group[13]-13-12"><label class="list-label" for="mce-group[13]-13-12">Instruction</label>
			    	<i class="fa fa-question-circle" onclick="" aria-hidden="true">
				    	<div class="sign-up-popover" onclick="">
				    		<h4>Instruction</h4>
				    		<p>The instruction-focused e-newsletter debuts in 2016 and will bring quick game improvement videos straight to the inboxes of serious golfers.</p>
				    	</div>
				    </i>
			    </div>
		    </li>
		    <li>
		    	<div class="item-wrap">
			    	<input checked="checked" type="checkbox" value="4" name="group[13][4]" id="mce-group[13]-13-2"><label class="list-label" for="mce-group[13]-13-2">Crash Course/Amateur Summer</label>
			    	<i class="fa fa-question-circle" onclick="" aria-hidden="true">
			    		<div class="sign-up-popover" onclick="">
			    			<h4>Crash Course/Amateur Summer</h4>
			    			<p>This weekly rundown switches focus seasonally to shine a spotlight on top competitors in the world of amateur and collegiate golf.</p>
			    		</div>
			    	</i>
		    	</div>
		    </li>
		</ul>
		
	</div>

	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>	
		
	<div class="clear"><button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">Subscribe for free</button></div>
		
	</form>
</body>
</html>

<!-- <div class="sign-up"> <div class="sign-up-title-container"> <h2 class="sign-up-title"><span class="icon-golfweek-icons_Golfweeklogo"></span></h2> <h3 class="sign-up-subtitle">Newsletters & E-Publications</h3> <h4 class="sign-up-subtext">Subscribe to Golfweek’s newsletters to receive  news and exclusive content</h4> </div> <form action="http://golfweek.us2.list-manage.com/subscribe/post?u=e8c5029f9dfb9d400e405c5c8&amp;id=a04fca388b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank"> <div class="mc-field-group email-fields"> <label class= "list-number" for="mce-EMAIL">1</label> <input type="email" value="" name="EMAIL" class="required-email" id="mce-EMAIL" placeholder="Enter your email address"> </div> <div class="mc-field-group input-group"> <label class="list-number">2</label> <h3>Select all the products you'd like to receive: </h3> <ul> <li> <div class="item-wrap"> <!-- <div class="checkbox-mobile"> --> <input type="checkbox" checked="checked" value="128" name="group[13][128]" id="mce-group[13]-13-7"><label class="list-label" for="mce-group[13]-13-7">Toy Box Extra</label> <!-- 						<label class="checkbox-mobile-label" for="checkbox-mobile"></label> </div> --> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Toy Box Extra</h4> <p>This gear-centric e-magazine delivers the latest golf equipment headlines and news every Wednesday.</p> </div> </i> </div> </li> <li class="right-col-list"> <div class="item-wrap"> <input checked="checked" type="checkbox" value="1" name="group[13][1]" id="mce-group[13]-13-0"><label class="list-label" for="mce-group[13]-13-0">Approach Shots</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Approach Shots</h4> <p>This daily update delivers the game's hottest headlines with a special emphasis on professional golf.</p> </div> </i> </div> </li> <li> <div class="item-wrap"> <input type="checkbox" checked="checked" value="1024" name="group[13][1024]" id="mce-group[13]-13-10"><label class="list-label" for="mce-group[13]-13-10">Ryder Cup + Majors Dailies</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Ryder Cup + Majors Dailies</h4> <p>With its new Ryder Cup and majors e-magazines, Golfweek equips fans with insider news and information leading up to, during and after these key events.</p> </div> </i> </div> </li> <li class="right-col-list"> <div class="item-wrap"> <input checked="checked" type="checkbox" value="16" name="group[13][16]" id="mce-group[13]-13-4"><label class="list-label" for="mce-group[13]-13-4">The Golf Life</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>The Golf Life</h4> <p>This golf travel/architecture guide is sent twice per week and offers tips on the best places to stay and play.</p> </div> </i> </div> </li> <li> <div class="item-wrap"> <input checked="checked" type="checkbox" value="2" name="group[13][2]" id="mce-group[13]-13-1"><label class="list-label" for="mce-group[13]-13-1">Junior Extra</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Junior Extra</h4> <p>This weekly report provides the exclusive, inside scoop on the next generation of rising golf stars.</p> </div> </i> </div> </li> <li class="right-col-list"> <div class="item-wrap"> <input checked="checked" type="checkbox" value="4096" name="group[13][4096]" id="mce-group[13]-13-12"><label class="list-label" for="mce-group[13]-13-12">Instruction</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Instruction</h4> <p>The instruction-focused e-newsletter debuts in 2016 and will bring quick game improvement videos straight to the inboxes of serious golfers.</p> </div> </i> </div> </li> <li> <div class="item-wrap"> <input checked="checked" type="checkbox" value="4" name="group[13][4]" id="mce-group[13]-13-2"><label class="list-label" for="mce-group[13]-13-2">Crash Course/Amateur Summer</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Crash Course/Amateur Summer</h4> <p>This weekly rundown switches focus seasonally to shine a spotlight on top competitors in the world of amateur and collegiate golf.</p> </div> </i> </div> </li> </ul> </div> <div id="mce-responses" class="clear"> <div class="response" id="mce-error-response" style="display:none"></div> <div class="response" id="mce-success-response" style="display:none"></div> </div> <div class="clear"><button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">Subscribe for free</button></div> </form> </div> -->