GOLFWEEK.GALLERYMODAL = {
	$galleryPreviews: $('.gallery-preview a'),
	$galleryModal: $('#gw-modal-gallery'),
	$galleryModalContent: $('#gw-modal-gallery .modal-content'),
	$galleryModalCloseButton: $('#gw-modal-gallery .modal-inner .close-button-container i'),
	scrollDownPage: 0,
	init: function() {
		var self = this;

		self.$galleryPreviews.on( 'click', function(e) {
			e.preventDefault();
			var galleryId = $(this).attr('data-gallery-id');

			$.ajax({
				url: ajaxgallery.ajaxurl,
				type: 'post',
				data: {
					action: 'ajax_gallery',
					gallery_id: galleryId
				},
				success: function(html) {
					self.$galleryModal.addClass('active');
					self.$galleryModal.animate({opacity:1}, 200);
					self.$galleryModalContent.html(html);
					self.activateModal();
				}
			})
		});

		self.$galleryModalCloseButton.on('click', function(e) {
			self.deactivateModal();
		});
	},
	activateModal: function() {
		this.scrollDownPage = $(window).scrollTop();
		GOLFWEEK.GALLERY.init({isGalleryDetailPage:false});
		GOLFWEEK.SHARE.facebookTrigger();
	},
	deactivateModal: function() {
		var self = this;
		$(window).scrollTop(this.scrollDownPage);
		self.$galleryModal.animate({opacity:0}, 200, function(){
			self.$galleryModal.removeClass('active');
			self.$galleryModalContent.html('');
			GOLFWEEK.GALLERY.destroy();
		});
	}
}

$(document).ready(function(){
  GOLFWEEK.GALLERYMODAL.init();
});