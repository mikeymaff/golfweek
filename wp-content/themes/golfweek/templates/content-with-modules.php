<section class="content-and-title-container">
	<div class="content-with-modules-title-container">
		<h1 class="content-with-modules-title"><?php echo the_title(); ?></h1>
	</div>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="content-container">
				<?php the_content(); ?>

			</div>
		<?php endwhile; ?>
	<?php endif; ?>		
</section>

<section class="right-modules">
	<?php include(locate_template('partials/module-builder.php')); ?>
</section>
