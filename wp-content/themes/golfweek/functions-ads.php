<?php
	// GW network
	define('GW_ADS_SLOT_LB', 'gw_leaderboard');
	define('GW_ADS_SLOT_LB_BOTTOM', 'gw_leaderboard-bottom');
	define('GW_ADS_SLOT_MODAL', 'gw_modal');
	define('GW_ADS_SLOT_RAIL', 'gw_rail');
	define('GW_ADS_SLOT_SKIN', 'gw_wallpaper');
	define('GW_ADS_SLOT_ARTICLE', 'gw_article');

	// test network
	// define('GW_ADS_SLOT_LB', '_test-leaderboard');
	// define('GW_ADS_SLOT_LB_BOTTOM', '_test-leaderboard-bottom');
	// define('GW_ADS_SLOT_MODAL', '_test-modal');
	// define('GW_ADS_SLOT_RAIL', '_test-rail');
	// define('GW_ADS_SLOT_SKIN', '_test-wallpaper');

	function gw_ads_get_mapped_path () {
		$ads_path = $_SERVER['REQUEST_URI'];

		if (preg_match('/^\/professional*/', $ads_path) !== false) {
			return '/pro-rankings';
		} else if (preg_match('/^\/college*/', $ads_path) !== false) {
			return '/college-golf';
		}

		return $ads_path;
	}


	function gw_ads_implode_categories ($category) {
		$categories = array();
		$categories[] = $category -> slug;

		if ($category -> parent) {// parent
			$parent = get_category($category -> parent);
			$categories[] = $parent -> slug;

			if ($parent->parent) {// grandparent
				$grandparent = get_category($parent -> parent);
				$categories[] = $grandparent -> slug;
			}
		}

		return implode(',', $categories);
	}


	function gw_ads_implode_tags () {
		$the_tags = get_the_tags();
		if (empty($the_tags)) {
			$the_tags = array();
		}

		$the_terms = get_the_terms('', 'player');
		if (empty($the_terms)) {
			$the_terms = array();
		}

		$all_terms = array_merge($the_tags, $the_terms);

		$tags = array();
		foreach ($all_terms as $term) {
			$tags[] = $term -> slug;
		}

		return implode(',', $tags);
	}


	$gw_ads_rail_count = 0;
	$gw_ads_article_count = 0;
	function gw_ads_get_dfp_html ($type, $slot, $size_request) {
		global $gw_ads_rail_count;
		global $gw_ads_article_count;

		$queried_object = get_queried_object();
		$page_template = str_replace('.php', '', basename(get_page_template()));

		// slight hack... "$gw_ads_rail_count" assumes that "rail" type modules are
		// repeated and require that their slot names be incremented - this will happen
		// by appending "-N" to the slot name where N = the current count...
		// the caller of "gw_ads_get_dfp_html" will assume that this occurs and therefore
		// does not track rail counts elsewhere
		if ($type === 'rail') {
			$gw_ads_rail_count++;
			$slot = $slot .'-'. $gw_ads_rail_count;
		}

		if ($type === 'article') {
			$gw_ads_article_count++;
			$slot = $slot .'-'. $gw_ads_article_count;
		}

		$targeting = array();
		if (is_home() || $page_template === 'page-home') {
			$targeting['page'] = 'home';
		} elseif (is_page()) {
			$targeting['page'] = 'page';
			$targeting['slug'] = $queried_object -> post_name;
		} elseif (is_tax() || is_tag() || is_category()) {
			$targeting['page'] = 'category';
			$targeting['slug'] = $queried_object -> slug;
		} elseif (is_single()) {
			$targeting['page'] = 'post';
			$targeting['slug'] = $queried_object -> post_name;
			$targeting['categories'] = gw_ads_implode_categories(get_the_category()[0]);
			$targeting['tags'] = gw_ads_implode_tags();

			// do not set empty tags
			if (empty($targeting['tags'])) {
				unset($targeting['tags']);
			}
		}

		$attributes = array();
		$attributes['data-type'] = $type;
		$attributes['data-slot'] = $slot;
		$attributes['data-size-request'] = $size_request;
		foreach ($targeting as $k => $v) {
			$attributes['data-target-'. $k] = $v;
		}

		$html = '<div class="gw-ad '. $type .'"';
		foreach ($attributes as $k => $v) {
			$html .= (' '. $k .'="'. $v .'"');
		}
		$html .= '></div>';
	
		return $html;
	}








