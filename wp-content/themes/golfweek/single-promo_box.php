<?php
get_header(); ?>

<?php $promo_box = get_queried_object(); ?>
<div class="module promo-box-module single-promo-box">
  <?php include(locate_template('modules/promo-box.php')); ?>
</div>

<?php
get_footer(); ?>