var $ = jQuery;
var GOLFWEEK = {};

var newsletterHTML = (
  '<div class="modal-inner sign-up-inner">' +
    '<div class="modal-close"><span class="icon-golfweek-icons_close"></span></div>' +
    '<div class="modal-content" style="overflow:auto;-webkit-overflow-scrolling:touch">' +
      '<div class="sign-up"> <div class="sign-up-title-container"> <h2 class="sign-up-title"><span class="icon-golfweek-icons_Golfweeklogo"></span></h2> <h3 class="sign-up-subtitle">Newsletters & E-Publications</h3> <h4 class="sign-up-subtext">Subscribe to Golfweek’s newsletters to receive  news and exclusive content</h4> </div> <form action="http://golfweek.us2.list-manage.com/subscribe/post?u=e8c5029f9dfb9d400e405c5c8&amp;id=a04fca388b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank"> <div class="mc-field-group email-fields"> <label class= "list-number" for="mce-EMAIL">1</label> <input type="email" value="" name="EMAIL" class="required-email" id="mce-EMAIL" placeholder="Enter your email address"> </div> <div class="mc-field-group input-group"> <label class="list-number">2</label> <h3>Select all the products you&rsquo;d like to receive: </h3> <ul> <li> <div class="item-wrap"> <!-- <div class="checkbox-mobile"> --> <input type="checkbox" checked="checked" value="128" name="group[13][128]" id="mce-group[13]-13-7"><label class="list-label" for="mce-group[13]-13-7">Toy Box Extra</label> <!--            <label class="checkbox-mobile-label" for="checkbox-mobile"></label> </div> --> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Toy Box Extra</h4> <p>This gear-centric e-magazine delivers the latest golf equipment headlines and news every Wednesday.</p> </div> </i> </div> </li> <li class="right-col-list"> <div class="item-wrap"> <input checked="checked" type="checkbox" value="1" name="group[13][1]" id="mce-group[13]-13-0"><label class="list-label" for="mce-group[13]-13-0">Approach Shots</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Approach Shots</h4> <p>This daily update delivers the game&rsquo;s hottest headlines with a special emphasis on professional golf.</p> </div> </i> </div> </li> <li> <div class="item-wrap"> <input type="checkbox" checked="checked" value="1024" name="group[13][1024]" id="mce-group[13]-13-10"><label class="list-label" for="mce-group[13]-13-10">Ryder Cup + Majors Dailies</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Ryder Cup + Majors Dailies</h4> <p>With its new Ryder Cup and majors e-magazines, Golfweek equips fans with insider news and information leading up to, during and after these key events.</p> </div> </i> </div> </li> <li class="right-col-list"> <div class="item-wrap"> <input checked="checked" type="checkbox" value="16" name="group[13][16]" id="mce-group[13]-13-4"><label class="list-label" for="mce-group[13]-13-4">The Golf Life</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>The Golf Life</h4> <p>This golf travel/architecture guide is sent twice per week and offers tips on the best places to stay and play.</p> </div> </i> </div> </li> <li> <div class="item-wrap"> <input checked="checked" type="checkbox" value="2" name="group[13][2]" id="mce-group[13]-13-1"><label class="list-label" for="mce-group[13]-13-1">Junior Extra</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Junior Extra</h4> <p>This weekly report provides the exclusive, inside scoop on the next generation of rising golf stars.</p> </div> </i> </div> </li> <li class="right-col-list"> <div class="item-wrap"> <input checked="checked" type="checkbox" value="4096" name="group[13][4096]" id="mce-group[13]-13-12"><label class="list-label" for="mce-group[13]-13-12">Instruction</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Instruction</h4> <p>The instruction-focused e-newsletter debuts in 2016 and will bring quick game improvement videos straight to the inboxes of serious golfers.</p> </div> </i> </div> </li> <li> <div class="item-wrap"> <input checked="checked" type="checkbox" value="4" name="group[13][4]" id="mce-group[13]-13-2"><label class="list-label" for="mce-group[13]-13-2">Crash Course/Amateur Summer</label> <i class="fa fa-question-circle" onclick="" aria-hidden="true"> <div class="sign-up-popover" onclick=""> <h4>Crash Course/Amateur Summer</h4> <p>This weekly rundown switches focus seasonally to shine a spotlight on top competitors in the world of amateur and collegiate golf.</p> </div> </i> </div> </li> </ul> </div> <div id="mce-responses" class="clear"> <div class="response" id="mce-error-response" style="display:none"></div> <div class="response" id="mce-success-response" style="display:none"></div> </div> <div class="clear"><button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">Subscribe for free</button></div> </form> </div>' +
    '</div>' +
  '</div>' +
  '<div class="modal-curtain"></div>'
);

GOLFWEEK.UTILS = {
  getParameterByName: function(name, url) {
      if (!url) url = window.location.href;
      url = url.toLowerCase();
      name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
}

GOLFWEEK.LANDINGPAGE = {
  $bigAndSmall: $('.big-and-small-features'),
  $big: $('.big-feature'),
  $med: $('.medium-features'),

  init: function(data) {
    this.resetHeights(); 
    this.setMaxHeights();

  },

  resize: function(px) {
    if (px < 1024) {
      this.resetHeights();
    } else {
      this.resetHeights();
      this.setMaxHeights();
    }
  },

  resetHeights: function() {
     this.$bigAndSmall.css("height", "");
     this.$big.css("height", "");
     this.$med.css("height", "");

   },

   setMaxHeights: function() {
       var bigAndSmallHeight = this.$bigAndSmall.height();
       var bigHeight = this.$big.height();
       var medHeight = this.$med.height();

       var maxHeight = Math.max(bigAndSmallHeight, bigHeight, medHeight);

       this.$bigAndSmall.height(maxHeight);
       this.$big.height(maxHeight);
       this.$med.height(maxHeight);

   }

}

GOLFWEEK.GALLERY = {
  $swiperSlides: $('.swiper-container .swiper-slide'),
  $figCaptions: $('.gallery-container .figcaption-container figcaption'),
  mySwiper: {},
  doPushState: true,
  init: function(data) {
    var self = this;

    self.$swiperSlides = $('.swiper-container .swiper-slide');
    self.$figCaptions = $('.gallery-container .figcaption-container figcaption');

    var startSlide = startSlide = parseInt(GOLFWEEK.UTILS.getParameterByName('slide')) - 1 || 0;

    self.mySwiper = new Swiper ('.swiper-container', {
      loop:true,
      initialSlide: startSlide,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      pagination: '.swiper-pagination',
      paginationType: 'fraction',
      keyboardControl: true
    });

    self.mySwiper.on('slideChangeEnd', function () {
      var numberOfSlides = self.$swiperSlides.length;
      var trueSlideIndex = self.mySwiper.activeIndex;
      if(trueSlideIndex == 0) {
        trueSlideIndex = numberOfSlides;
      } else if(trueSlideIndex == numberOfSlides + 1) {
        trueSlideIndex = 1;
      }
      
      if(data.isGalleryDetailPage) {
        if(self.doPushState) {
          history.pushState({slide:trueSlideIndex}, '', '?slide=' + trueSlideIndex);
        }
        self.doPushState = true;
      }

      self.$figCaptions.removeClass('show');
      self.$figCaptions.eq(trueSlideIndex - 1).addClass('show');
    });

    self.$figCaptions.eq(startSlide).addClass('show');
  },
  onPopState: function(slideIndex) {
    this.doPushState = false;
    this.mySwiper.slideTo(slideIndex);
  },
  destroy: function() {
    this.mySwiper.destroy(true, true);
    this.mySwiper = {};
  }
}

GOLFWEEK.HEADER = {
  $navbar: $('.site-header'),
  $parentCategories: $('#menu-main-nav').children('.menu-item-has-children'),
  $dropdownTrigger: $('#menu-main-nav .menu-item-has-children .dropdown-trigger'),
  $categoryLink: $('.sub-menu li'),
  $searchTrigger: $('.search-toggle'),
  $closeSearchTrigger: $('.input-container .icon-golfweek-icons_close'),
  $mobileNavTrigger: $('.nav-toggle.mobile'),
  $headerTop: $('.site-header'),
  $navCurtain: $('.nav-curtain'),
  init: function() {
    var self = this;

    self.$parentCategories.each(function(){
      $(this).append('<span class="dropdown-trigger"></span>');
    });

    this.$searchTrigger.on('click', function(e) {
      self.toggleSearchInput();
      self.autoFocusSearch();
    });

    this.$closeSearchTrigger.on('click', function(e) {
      self.toggleSearchInput();
    });

    this.$parentCategories.on('click', function(e) {
      if($(window).width() >= 768) {
        return;
      }
      
      var $this = $(this);
      
      if($this.children('.dropdown-trigger')[0] == e.target) {
        self.toggleMobileSubcategories($this);
        return;
      }
      // on first click, prevent default and toggle submenu
      // on second click, go to the appropriate link
      if(!$this.hasClass('active-open')) {
        e.preventDefault();
        self.toggleMobileSubcategories($this);
      }
    });

    this.$mobileNavTrigger.on('click', function(e) {
      self.toggleMobileNav();
    });

    this.$categoryLink.on('click', function(e) {
      e.stopPropagation();
    });

    this.$navCurtain.on('click', function(e) {
      self.toggleMobileNav();
    });

  },
  toggleMobileNav: function() {
    this.$headerTop.toggleClass('mobile-expanded');
    this.$navCurtain.toggleClass('active');
  },
  toggleMobileSubcategories: function($parentTarget) {
    var isCurrentlyOpen = $parentTarget.hasClass('active-open');

    this.$parentCategories.removeClass('active-open');

    if (!isCurrentlyOpen) {
      $parentTarget.addClass('active-open');
    }
  },
  toggleSearchInput: function() {
    var $inputContainer = $('.search-container');

    $inputContainer.toggleClass('search-open');
  },
  autoFocusSearch: function() {

    var $searchBox = $('#site-search-box');
    $searchBox.focus();

  },

  scroll: function(px) {
    var isFixed = this.$navbar.hasClass('fixed');

    if ((px < 121 && !isFixed) || (px > 121 && isFixed)) {
      return;
    } else if (px < 121  && isFixed) {
      this.$navbar.removeClass('fixed');
    } else if (px > 121 && !isFixed) {
      this.$navbar.addClass('fixed');
    }
  },
  resize: function(px) {
    if (px < 767) {
      return;
    }

    this.$parentCategories.removeClass('active-open');
    this.$headerTop.removeClass('mobile-expanded');
  }
};

GOLFWEEK.MODAL = {
  $newsLetterModalTrigger: $('#newsletter-signup'),
  $modal: $('#gw-modal'),
  $body: $('body'),
  init: function() {
    var self = this;

    this.$newsLetterModalTrigger.on('click', function() {
      self.setModalOpen(newsletterHTML);
    });

    this.$modal.on('click', '.modal-close, .modal-continue', function() {
      self.setModalClosed();
    });

    this.$modal.on('click', '.modal-curtain', function() {
      self.setModalClosed();
    });

  },
  setModalOpen: function(html) {
    this.$body.addClass('modal-open');
    this.$modal.append(html);
  },
  setModalClosed: function() {
    this.$modal.empty();
    this.$body.removeClass('modal-open');
  }
};

GOLFWEEK.COMMENTS = {
  $showHideButton: $('.comments-container .comments-header .show-hide-button'),
  $commentsContainer: $('.comments-container'),
  init: function() {
    var self = this;
    self.$showHideButton.click(function(){
      if(self.$commentsContainer.hasClass('open')) {
        self.$commentsContainer.removeClass('open');
      } else {
        self.$commentsContainer.addClass('open');
      }
    });
  }
}

GOLFWEEK.SHARE = {
  init: function() {
    this.facebook();
    this.twitter();
  },
  facebook: function() {
    window.fbAsyncInit = function() {
      FB.init({
        appId: 1646295555626359,
        xfbml: true,
        version: 'v2.5'
      });

      $('.facebook-share').on('click', function() {
        FB.ui({
          method: 'share',
          href: $(this).data('url')
        }, function(response){

        });
      });
    };

    (function(d, s, id) {
      var js,
          fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));
  },
  twitter: function() {
    window.twttr = (function(d, s, id) {
      var js,
          fjs = d.getElementsByTagName(s)[0],
          t = window.twttr || {};

      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);
      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };

      return t;
    } (document, "script", "twitter-wjs"));
  },
  facebookTrigger: function() {
    $('.facebook-share').on('click', function() {
      FB.ui({
        method: 'share',
        href: $(this).data('url')
      }, function(response){

      });
    });
  }
};

GOLFWEEK.SUBFEATURED = {
  init: function(px) {
    this.setAllToSameHeight(px);
  },
  setAllToSameHeight: function(px) {
    var $categoryList = $('.sub-featured-category');
    var height = 0;

    $categoryList.height('auto');

    if (px < 768) {
      return;
    }

    $categoryList.each(function(i, list) {
      if ($(list).height() > height) {
        height = $(list).height();
      }
    });

    $categoryList.height(height);
  },
  resize: function(px) {
    var self = this;
    var resizeThreshold;

    clearTimeout(resizeThreshold);
    resizeThreshold = setTimeout(function() {
      self.setAllToSameHeight(px);
    }, 500);
  }
};

GOLFWEEK.ADS = {
  // cookie name (not the actual count)
  COOKIE_MODAL_COUNT: 'gw-modal-count',

  // modal configuration
  MODAL_COUNT_MAX: 1,

  // refresh configuration
  DFP_REFRESHES: true,

  DFP_SLOT_PREFIX: '/310322/a.site152.tmus/',
  DFP_TARGET_READ_PREFIX: 'target',
  DFP_TARGET_SEND_PREFIX: 'gw',

  // used for debugging ad targeting in console
  debugLog: [ ],

  // we must defer the modal ad for the site
  // because it should not display if not needed
  deferredModalDivId: null,

  // assumes only 1 wallpaper at a time will be
  // served by DFP
  wallpaperSlot: null,

  init: function () {
    var adDivIds = [ ];
    var adRailCount = 0;

    googletag.cmd.push(function () {
      var adSizeMappings = {
        'leaderboard': googletag.sizeMapping().
          addSize([0, 0], [[320, 50], [300, 50]]).
          addSize([768, 0], [728, 90]).
          addSize([1020, 0], [[970, 90], [970, 250], [728, 90]]).
          build(),

        'modal': googletag.sizeMapping().
          addSize([0, 0], [300, 250]).
          addSize([768, 0], [650, 500]).
          build(),

        'rail': googletag.sizeMapping().
          addSize([0, 0], [300, 250]).
          addSize([768, 0], [[300, 250], [300, 600]]).
          build(),

        'rail-first': googletag.sizeMapping().
          addSize([0, 0], [300, 250]).
          addSize([768, 0], [300, 250]).
          build(),

        'skin': googletag.sizeMapping().
          addSize([0, 0], []).
          addSize([768, 0], [2200, 1100]).
          build(),

        'default': googletag.sizeMapping().
          addSize([0, 0], [300, 250]).
          addSize([768, 0], [[300, 250], [300, 600]]).
          build(),

        'article': googletag.sizeMapping().
          addSize([0, 0], [300, 250]).
          build()
      };

      $('.gw-ad').each(function (index) {
        var $ad = $(this);
        var adDivId = 'gwd-ad-'+ index;
        var adType = $ad.data('type');
        var adSlotName = GOLFWEEK.ADS.DFP_SLOT_PREFIX + $ad.data('slot');

        var adSizeRequest = GOLFWEEK.ADS.parseSizeRequestFromString($ad.data('size-request')) || [300, 250];
        var adSlotTargeting = GOLFWEEK.ADS.parseTargetingFromAd($ad);
        var adSizeMapping = adSizeMappings[adType] || adSizeMappings['default'];

        GOLFWEEK.ADS.debugLog.push('--------------');
        GOLFWEEK.ADS.debugLog.push('div:'+"\t"+'['+ adDivId +']');
        GOLFWEEK.ADS.debugLog.push('slot:'+"\t"+ adSlotName);

        // rails are always sized the same EXCEPT for the
        // first rail on the homepage... as such prevent first
        // rail on the homepage from serving 300x600 by using
        // a different sizing mapping (rail-first)
        if (GOLFWEEK.ADS.isHomepage() && adType === 'rail') {
          adRailCount++;
          if (adRailCount === 1) {
            adSizeMapping = adSizeMappings['rail-first'];
            adSizeRequest = [300, 250];
            GOLFWEEK.ADS.debugLog.push('!!!:'+"\t"+'['+ adDivId +'] first rail on homepage size mapping changed');
          }
        }

        GOLFWEEK.ADS.debugLog.push('size:'+"\t"+ adSizeRequest);

        // we only want to define slots for a modal here
        // they will be displayed only when the modal appears
        if (adType === 'modal') {
          GOLFWEEK.ADS.deferredModalDivId = adDivId;
        } else {
          adDivIds.push(adDivId);
          $ad.html('<div class="gw-ad__inner" id="'+ adDivId +'">');
        }

        var adSlot = googletag.defineSlot(adSlotName, adSizeRequest, adDivId).addService(googletag.pubads());
        adSlot.defineSizeMapping(adSizeMapping);
        for (var i in adSlotTargeting) {
          GOLFWEEK.ADS.debugLog.push('t-kv:'+"\t"+ i +'='+ adSlotTargeting[i]);
          adSlot.setTargeting(i, adSlotTargeting[i]);
        }

        // remember the "skin" (wallpaper) slot so it can be used for later
        // matching in slot rendering logic
        if (adType === 'skin') {
          GOLFWEEK.ADS.wallpaperSlot = adSlot;
        }

        // debug deferred modal id
        if (GOLFWEEK.ADS.deferredModalDivId) {
          GOLFWEEK.ADS.debugLog.push('deferring ['+ GOLFWEEK.ADS.deferredModalDivId +'] for modal');
        }
      });

      // listen for slot rendering
      googletag.pubads().addEventListener('slotRenderEnded', function(event) {
        GOLFWEEK.ADS.onSlotRendered(event);
      });

      googletag.pubads().enableSingleRequest();
      googletag.enableServices();

      // refresh ads on an interval if necessary
      if (GOLFWEEK.ADS.DFP_REFRESHES) {
        setInterval(function () {
          googletag.cmd.push(googletag.pubads().refresh());
        }, GOLFWEEK.ADS.refreshRateInMilliseconds());
        GOLFWEEK.ADS.debugLog.push("\n"+ 'refreshing at ['+ GOLFWEEK.ADS.refreshRateInMilliseconds() +']');
      }

      // check if we need to display a modal
      GOLFWEEK.ADS.triggerModalIfNeeded();
    });

    googletag.cmd.push(function () {
      for (var i=0; i<adDivIds.length; i++) {
        googletag.display(adDivIds[i]);
      }
    });
  },

  onSlotRendered: function (event) {
    if (event.slot === this.wallpaperSlot && !event.isEmpty) {
      $('body').addClass('with-skin');
    }
  },

  triggerModalIfNeeded: function () {
    // no need if we are on the homepage
    if (this.isHomepage() || this.is404page()) {
      return;
    }

    // check cookies for max count
    var modalCountCookie = Cookies.get(this.COOKIE_MODAL_COUNT);
    var modalCount = modalCountCookie ? parseInt(modalCountCookie, 10) : 0;
    if (modalCount >= this.MODAL_COUNT_MAX) {
      return;
    }

    // if we got here, we need a modal
    modalCount++;
    GOLFWEEK.MODAL.setModalOpen($('#gw-modal-template-ad').html());
    Cookies.set(this.COOKIE_MODAL_COUNT, modalCount, { expires: this.modalCookieExpiresDate() });

    // discard old ad information
    var $modal = $('#gw-modal');
    $modal.find('.gw-ad').remove();

    // build a div with the deferred modal id and display it
    $modal.find('.modal-content-inner').append('<div class="dfp-wrapper" id="'+ this.deferredModalDivId +'"></div>');
    setTimeout(function () {
      googletag.cmd.push(function () {
        googletag.display(GOLFWEEK.ADS.deferredModalDivId);
      });
    }, 100);
  },

  parseSizeRequestFromString: function (string) {
    var sizes = [];
    var tokens = string ? string.split(',') : [];
    for (var i=0; i<tokens.length; i++) {
      var dimensions = [];
      var numberStrings = tokens[i].split('x');
      dimensions.push(parseInt(numberStrings[0], 10));
      dimensions.push(parseInt(numberStrings[1], 10));
      sizes.push(dimensions);
    }
    return sizes;
  },

  parseTargetingFromAd: function ($ad) {
    var allData = $ad.data();
    var targetingData = { };
    for (var i in allData) {
      if (i.indexOf(this.DFP_TARGET_READ_PREFIX) === 0) {
        targetingData[this.DFP_TARGET_SEND_PREFIX + i.replace(this.DFP_TARGET_READ_PREFIX, '')] = allData[i];
      }
    }
    return targetingData;
  },

  safePath: function () {
    return GOLFWEEK_PATH && GOLFWEEK_PATH !== '/' ? GOLFWEEK_PATH : '';
  },

  refreshRateInMilliseconds: function () {
    return this.isHomepage() ? 60000 : 150000;
  },

  isHomepage: function () {
    var relativePath = document.location && document.location.pathname ? document.location.pathname : '/not-home';
    return relativePath === '/';
  },

  is404page: function () {
    // is404Page is set globally on a page <script> tag
    return typeof is404Page !== 'undefined' && is404Page;
  },

  redoModal: function () {
    Cookies.remove(this.COOKIE_MODAL_COUNT);
  },

  modalCookieExpiresDate: function () {
    var expiresOn = new Date();
    expiresOn.setTime(expiresOn.getTime() + (36 * 3600 * 1000));/* 36 hours */
    return expiresOn;
  },

  debug: function () {
    console.log(this.debugLog.join("\n"));
  }
};

GOLFWEEK.ARTICLE = {
  init: function() {
    var self = this;
    var onArticlePage = $('main').hasClass('single-post');

    if(onArticlePage) {
      self.makeAdFloat();
    }
  },
  makeAdFloat: function() {
    // an ad should only be floated if it's followed by a p
    $('.ad-in-article').each(function(){
      var $this = $(this);
      if($this.next('p').length > 0) {
        $this.addClass('float-in-article')
      };
    });
  }
}

window.onpopstate = function(event) {
  // console.log(event);
  if(event.state) {
    GOLFWEEK.GALLERY.onPopState(event.state.slide);
  } else {
    GOLFWEEK.GALLERY.onPopState(1);
  }
};

GOLFWEEK.SHARE.init();

$(document).ready(function(){
  GOLFWEEK.SUBFEATURED.init($(window).width());
  GOLFWEEK.HEADER.init();
  GOLFWEEK.MODAL.init();
  GOLFWEEK.COMMENTS.init();
  GOLFWEEK.ADS.init();
  GOLFWEEK.LANDINGPAGE.init();
  GOLFWEEK.ARTICLE.init();
});

$(window).scroll(function() {
  GOLFWEEK.HEADER.scroll(this.scrollY);
});

var resizeThreshold;
$(window).resize(function() {
  clearTimeout(resizeThreshold);
  resizeThreshold = setTimeout(function() {
    GOLFWEEK.SUBFEATURED.resize($(window).width());
    GOLFWEEK.HEADER.resize($(window).width());
    GOLFWEEK.LANDINGPAGE.resize($(window).width());
  }, 100);
});

$(window).load(function() {
  GOLFWEEK.SUBFEATURED.init($(window).width());
  GOLFWEEK.LANDINGPAGE.init();
});