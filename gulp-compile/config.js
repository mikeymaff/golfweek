var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

module.exports.paths = {

  less: {
    src: '../wp-content/themes/golfweek/less/style.less',
    watch: '../wp-content/themes/golfweek/less/**/*.less'
  },

  js: {
    src: [
      '../wp-content/themes/golfweek/js/swiper.jquery.min.js',
      '../wp-content/themes/golfweek/js/js.cookie.js',
      '../wp-content/themes/golfweek/js/z_app.js'
    ],
    watch: '../wp-content/themes/golfweek/js/*.js'
  }

};

module.exports.options = {

  browserSync: {
    proxy: 'local.golfweek.com'
  },

  plumber: {
    errorHandler: function(err) {
      var border = '\n=============OH NOS!==============\n';

      $.util.beep();
      $.notify(err);
      console.log(err);
      console.log(border);
      console.log('File:' + err.fileName);
      console.log('Line:' + err.line);
      console.log(border);
      this.emit('end');
    }
  }

};
