<?php 
	get_header(); 

?>

<?php if (get_field('is_stats_page')){ ?>

	<?php if(get_field('include_right_rail')){ ?>
		<main class="page-<?php the_title(); ?> layout-content-with-modules">
			
			<?php include(locate_template('templates/content-with-modules.php')); ?>
		</main>
		<?php } else  { ?>
	
		<main class="page-<?php the_title(); ?> layout-content-without-modules">
			<?php include(locate_template('templates/content-without-modules.php')); ?>
		</main>
	<?php } ?>

<?php } else { ?>
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
				<main class="page">
					<div class="page-title-container">
						<h1 class="page-title">
							<?php the_title(); ?>	
						</h1>
					</div>
					<section class="page-content-container">
						<?php the_content(); ?>
					</section>
				</main>
		<?php endwhile; ?>
	<?php endif; ?>		
<?php } ?>
<?php get_footer(); ?>