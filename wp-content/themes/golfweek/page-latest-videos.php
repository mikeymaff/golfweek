<?php 
	get_header(); 
	$queried_object = get_queried_object();
	$content_type = 'video_post';
?>

<main class="article-list">
	<?php include(locate_template('templates/article-list.php')); ?>
</main>

<?php get_footer(); ?>
